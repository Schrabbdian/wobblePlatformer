﻿Shader "Custom/Deform/Basic Transparent" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Specular("Specular Color", Color) = (1, 1, 1, 1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_SpecularMap("Specular Map", 2D) = "white" {}
		_NormalMap("Normal Map", 2D) = "bump" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Emission("Emission Color", Color) = (1, 1, 1, 1)
		_SkinWidth("Skin Width", Range(0.01,0.5)) = 0.1

		//hidden properties
		[HideInInspector]_deformRotation("Deformation Rotation Angle", Range(-3.1415, 3.1415)) = 0
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 200
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf StandardSpecular fullforwardshadows vertex:vert alpha:premul
		#pragma target 5.0
		#include "UnityCG.cginc"
		#include "Assets/Graphical Assets/Shaders/Deformation/deformation.cginc"

			
		void vert (inout deformIn v) 
		{
			deformFunctionInPlace(v);
		}
		
		sampler2D _MainTex;
		sampler2D _NormalMap;
		sampler2D _SpecularMap;

		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_NormalMap;
			float2 uv_SpecularMap;
		};

		half _Glossiness;
		

		fixed4 _Specular;
		fixed4 _Color;
		half4 _Emission;
		
		void surf(Input IN, inout SurfaceOutputStandardSpecular o) 
		{
			//albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;

			//specular is a combination of specular color and specular map
			o.Specular = tex2D(_SpecularMap, IN.uv_SpecularMap) * _Specular.rgb;

			//normal map sampling
			o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
			
			//smoothness and emission are specified directly
			o.Smoothness = _Glossiness;
			o.Emission = _Emission.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
