#ifndef MESH_DEFORMATION
#define MESH_DEFORMATION

#if SHADER_TARGET >= 50
	uniform StructuredBuffer<float3> cagePoints;//in object space
	uniform StructuredBuffer<float> weights;//weights of cagePoints for each vertex
	uniform int cagePoints_count;
	uniform StructuredBuffer<float3> staticPosition;//original position of each vertex in local space
#else
	uniform sampler2D _VertexWeights;
	uniform sampler2D _StaticPositions;
	uniform int _static_pos_texture_size;
	uniform float4 cagePointsArr[512];
	uniform int cagePointsCount;
	uniform int vertexCount;
	uniform int _weights_texture_size;
	uniform float VAL_RANGE;
#endif

//structs
struct deformIn
{
	uint id : SV_VertexID;
	float4 vertex : POSITION;
	half4 tangent : TANGENT;
	half3 normal : NORMAL;
	float4 texcoord : TEXCOORD0;
	float4 texcoord1 : TEXCOORD1;
	float4 texcoord2 : TEXCOORD2;
	float4 texcoord3 : TEXCOORD3;
};

//properties
float _SkinWidth;
float _deformRotation;//The rotation angle induced by the deformation

// inplace deformes the given vertex v
void deformFunctionInPlace(inout deformIn v)
{
	// if structured buffers are accesible use this
	#if SHADER_TARGET >= 50
		float4 newPos = float4(0, 0, 0, 1.0f);
		float4 deltaPos = v.vertex - float4(staticPosition[v.id].xyz, 1);//calculate displacement by e.g. animation

		//calculate new Vertex position as linear combination of weighted cage point positions
		for(uint i = 0; i < cagePoints_count; i++)
		{
			float weight = weights[v.id * cagePoints_count + i];
			newPos.xy += weight * cagePoints[i].xy;
		}
	// otherwise use the ugly fallback mode
	#else

		float4 newPos = float4(0, 0, 0, 1.0f);

		// get the static positions
		// adding 0 is somehow very important... I don't know why
		int _static_pos_index = v.id*3+0.0f;

		// now get the correct indices for the positions
		int _static_pos_index_x_row = _static_pos_index / _static_pos_texture_size;
		int _static_pos_index_x_col = _static_pos_index % _static_pos_texture_size;
		int _static_pos_index_y_row = (_static_pos_index+1) / _static_pos_texture_size;
		int _static_pos_index_y_col = (_static_pos_index+1) % _static_pos_texture_size;
		int _static_pos_index_z_row = (_static_pos_index+2) / _static_pos_texture_size;
		int _static_pos_index_z_col = (_static_pos_index+2) % _static_pos_texture_size;

		// get the actutal positions from the texture
		float3 static_pos_x = tex2Dlod(_StaticPositions,float4((float(_static_pos_index_x_row)+0.5f)/float(_static_pos_texture_size),(float(_static_pos_index_x_col)+0.5f)/float(_static_pos_texture_size),0,0)).rgb;
		float3 static_pos_y = tex2Dlod(_StaticPositions,float4((float(_static_pos_index_y_row)+0.5f)/float(_static_pos_texture_size),(float(_static_pos_index_y_col)+0.5f)/float(_static_pos_texture_size),0,0)).rgb;
		float3 static_pos_z = tex2Dlod(_StaticPositions,float4((float(_static_pos_index_z_row)+0.5f)/float(_static_pos_texture_size),(float(_static_pos_index_z_col)+0.5f)/float(_static_pos_texture_size),0,0)).rgb;

		// rescale the values
		float x = (static_pos_x.r*2.0f-1.0f)*(static_pos_x.g+static_pos_x.b*VAL_RANGE);
		float y = (static_pos_y.r*2.0f-1.0f)*(static_pos_y.g+static_pos_y.b*VAL_RANGE);
		float z = (static_pos_z.r*2.0f-1.0f)*(static_pos_z.g+static_pos_z.b*VAL_RANGE);

		float4 deltaPos = v.vertex - float4(x,y,z,1);//calculate displacement by e.g. animation

		for(uint i = 0; i < cagePointsCount; i++)
		{
		    // also get the weights from the texture
		    int _index = v.id * cagePointsCount + i;
		    int _rgbaIndex = _index % 4;
            int _rowIndex = (_index / 4) / _weights_texture_size;
            int _colIndex = (_index / 4) % _weights_texture_size;
			float weight = tex2Dlod(_VertexWeights,float4((float(_rowIndex)+0.5f)/float(_weights_texture_size),(float(_colIndex)+0.5f)/float(_weights_texture_size),0,0))[_rgbaIndex];
			newPos.xy += weight * cagePointsArr[i].xy;
		}
	#endif


	//get rotation of cage
	float angle = _deformRotation;

	//apply same rotation to vertex-specific vectors (normal, deltaPos)
	float cos_a = cos(angle);
	float sin_a = sin(angle);
			
	//rotate deltaPos
	float4 deltaPos_r = deltaPos;
	deltaPos_r.x = deltaPos.x * cos_a + deltaPos.y * sin_a;
	deltaPos_r.y = -deltaPos.x * sin_a + deltaPos.y * cos_a; 

	//rotate normal
	float3 n = v.normal;
	n.x = v.normal.x * cos_a + v.normal.y * sin_a;
	n.y = -v.normal.x * sin_a + v.normal.y * cos_a; 

	//set output values
	v.vertex.xy = newPos.xy;
	v.vertex += deltaPos_r;//add displacement back onto new position
	v.vertex.xyz *= (1.0f + _SkinWidth);

	v.normal = n;
}

// only include the conversion method when included with the standard shader
#ifdef __STANDARD_SHADER__DEFORM_INCLUDE_FLAG
VertexInput deformFunction(in deformIn  v)
{
	VertexInput output;

	deformFunctionInPlace(v);

	//output in "VertexInput" format
	output.vertex = v.vertex;
	output.normal = v.normal;
	output.uv0 = v.texcoord;

	#ifndef UNITY_STANDARD_SHADOW_INCLUDED
		output.vertex = v.vertex;
		output.normal = v.normal;
		output.uv0 = v.texcoord;
		output.uv1 = v.texcoord1;
		
		#ifdef _TANGENT_TO_WORLD
			output.tangent = v.tangent;
		#endif

		#if defined(DYNAMICLIGHTMAP_ON) || defined(UNITY_PASS_META)
			output.uv2 = v.texcoord2;
		#endif
	#endif
	
	return output;
}
#endif

#endif