﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizeSwitch : MonoBehaviour
{
    public Collider2D[] colliders;
    public Renderer renderer;

    public float wallDistanceTolerance = 0.1f;

    [SerializeField]
    private MassSpringSystem commonSys = null;

    private Color originalColor;

	void Start () 
    {
        originalColor = renderer.material.color;
	}

    void FixedUpdate()
    {
        if (commonSys != null)
        {
            //check if the common system still touches all parts
            foreach (Collider2D collider in colliders)
            {
                if (!isTouching(collider, commonSys))
                {
                    //if it does not, remove common system
                    commonSys = null;
                    renderer.material.color = originalColor;
                    break;
                }
            }
        }


    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (commonSys != null) return;//only check if there is currently no common system

        MassPoint mp = other.GetComponent<MassPoint>();

        if(mp != null)
        {
            MassSpringSystem ms = mp.GetSystem();

            //check if the system touches all parts
            bool touchesAll = true; 

            foreach (Collider2D collider in colliders)
            {
                if (!isTouching(collider, ms)) 
                {
                    touchesAll = false;
                    break;
                }
            }

            if (touchesAll)
            {
                commonSys = ms;
                renderer.material.color = new Color(0, 1, 0, originalColor.a);
            }
        }
    }


    private bool isTouching(Collider2D coll, MassSpringSystem ms)
    {
        foreach (Collider2D c in ms.getPointColliders())
        {
            ColliderDistance2D dist = Physics2D.Distance(c, coll);

            if (dist.distance < wallDistanceTolerance)
            {
                return true;
            }
        }

        return false;
    }
}
