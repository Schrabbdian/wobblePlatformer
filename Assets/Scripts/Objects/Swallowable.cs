﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//parent class for all objects that can be swallowed
public abstract class Swallowable : MonoBehaviour, IFreezable
{
    [HideInInspector]
    public bool canSwallow = true;

    private Vector2 preSwallowScale;

    //Parameters
    public Vector2 sizeModifier;

    //Component References
    protected MassSpringSystem massSpring;
    protected Rigidbody2D rigidBody;
    protected Collider2D coll;


    //State
    public enum State
    {
        Ready,
        Inhaled,
        Swallowed,
        Spit
    }

    public State state { get; set; }

	//Initialization
	void Start () 
    {
		//set up component references
        rigidBody = GetComponent<Rigidbody2D>();
        massSpring = GetComponent<MassSpringSystem>();

        coll = GetComponent<Collider2D>();

        if (!(rigidBody != null ^ massSpring != null))
        {
            Debug.LogError("Swallowable Objects must have either a Rigidbody2D or a MassSpringSystem (not both)!");
        }

        if (coll == null)
        {
            Debug.LogError("Swallowable Objects must have a Collider2D!");
        }
	}


    public void SetActive(bool active)
    {
        if (massSpring != null)
        {
            massSpring.SetActive(active);
        }
        else
        {
            gameObject.SetActive(active);
        }

    }


    //Scale
    public void SetScale(Vector2 newScale)
    {
        if (massSpring != null)
        {
            massSpring.SetScale(newScale);
        }
        else
        {
            transform.localScale = new Vector3(newScale.x, newScale.y, transform.localScale.z);
        }
    }

    public Vector2 GetScale()
    {
        if (massSpring != null)
        {
            return massSpring.GetScale();
        }
        else
        {
            return transform.localScale;
        }
    }
	

    //Physics
    public float GetMass()
    {
        if (massSpring != null)
        {
            return massSpring.mass;
        }
        else
        {
            return rigidBody.mass;
        }
    }

    public void AddForce(Vector2 force)
    {
        if (massSpring != null)
        {
            massSpring.AddForce(force);
        }
        else
        {
            rigidBody.AddForce(force);
        }
    }

    public void SetPosition(Vector2 pos)
    {
        if (massSpring != null)
        {
            massSpring.SetPosition(pos);
        }
        else
        {
            transform.position = pos;
        }
    }

    public void SetIgnoreCollision(Collider2D collider, bool active)
    {
        if (massSpring != null)
        {
            massSpring.SetIgnoreCollision(collider, active);
        }
        else
        {
            Physics2D.IgnoreCollision(collider, coll, active);
        }
    }

    public void SetIgnoreCollision(MassSpringSystem ms, bool active)
    {
        if (massSpring != null)
        {
            massSpring.SetIgnoreCollision(ms, active);
        }
        else
        {
            ms.SetIgnoreCollision(coll, active);
        }
    }


    public bool OverlapCollider(Collider2D other, LayerMask layerM)
    {

        Collider2D[] results = new Collider2D[1000];

        ContactFilter2D filter = new ContactFilter2D();
        filter.SetLayerMask(layerM);
        filter.useTriggers = true;

        //check which "Inhalable" objects overlap with the specified collider
        int n = other.OverlapCollider(filter, results);

        //if our collider is one of these, there is an overlap
        for (int i = 0; i < n; i++)
        {
            if (results[i].Equals(coll)) { return true; }
        } 

        return false;
    }


    //Swallow Event Methods (These call the ones below and do additional stuff that ALWAYS has to be done)
    public IEnumerator SwallowRoutine(SwallowAbility swallower)
    {
        yield return swallower.StartCoroutine(OnSwallow(swallower));

        swallower.GetSwallowedObjects().Add(this);//add self to swallowed list
    }

    public IEnumerator SpitRoutine(SwallowAbility swallower, Vector2 spitDir, float delay)
    {
        state = State.Spit;

        yield return swallower.StartCoroutine(OnSpit(swallower, spitDir, delay));

        state = State.Ready;
    }

    public void InhaleTick(SwallowAbility inhaler)
    {
        state = State.Inhaled;

        WhileInhaled(inhaler);
    }

    public void SwallowedTick(SwallowAbility swallower)
    {
        state = State.Swallowed;

        WhileSwallowed(swallower);
    }



    //Swallow Methods (Override in Subclass)
    public virtual IEnumerator OnSwallow(SwallowAbility swallower)
    {
        //shrink object being swallowed
        Vector2 scale = GetScale();
        preSwallowScale = scale;//save scale to restore later
        canSwallow = false;


        while (scale.sqrMagnitude > 0.25f)
        {
            scale *= (1.0f - 10.0f * Time.deltaTime);
            SetScale(scale);
            yield return null;
        }


        SetActive(false);//disable object

        //adjust player scale based on what is swallowed
        MassSpringSystem ms = swallower.GetMassSpringSystem();

        ms.SetScale((Vector2)ms.GetScale() + sizeModifier);
    }

    public virtual IEnumerator OnSpit(SwallowAbility swallower, Vector2 spitDir, float delay)
    {
        yield return new WaitForSeconds(delay);

        SetPosition(swallower.transform.position);

        yield return new WaitForFixedUpdate();

        SetActive(true);
        SetIgnoreCollision(swallower.GetMassSpringSystem(), true);//ignore collisions with player
        swallower.GetIgnoredCollisions().Add(this);

        yield return new WaitForFixedUpdate();

        AddForce(spitDir.normalized * swallower.spitStrength * GetMass());//accelerate object


        //grow object being spit out
        Vector2 scale = GetScale();
        while (scale.x < preSwallowScale.x || scale.y < preSwallowScale.y)
        {
            scale *= (1.0f + 5.0f * Time.deltaTime);
            SetScale(scale);
            yield return null;
        }

        SetScale(preSwallowScale);


        //reactivate collision with player if far enough away
        PolygonCollider2D polyColl = swallower.GetPolygonCollider();
        while (OverlapCollider(polyColl, swallower.swallowLayer))
        {
            yield return new WaitForFixedUpdate();
        }

        SetIgnoreCollision(swallower.GetMassSpringSystem(), false);//stop ignoring collisions with player
        swallower.GetIgnoredCollisions().Remove(this);
        canSwallow = true;


    }

    public abstract void WhileInhaled(SwallowAbility inhaler);//called every frame while the swallowable is being inhaled
    
    public abstract void WhileSwallowed(SwallowAbility swallower);//called every frame while the swallowable is ingested

    public void OnFreeze()
    {
        canSwallow = false;
    }

    public void OnUnfreeze()
    {
        canSwallow = true;
    }

    public void OnFreezeChanged(float max, float current, float change)
    {

    }
}
