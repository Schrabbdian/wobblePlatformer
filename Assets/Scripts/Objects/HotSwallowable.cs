﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotSwallowable : Swallowable {

    public override void WhileInhaled(SwallowAbility inhaler)
    {
        
    }

    public override void WhileSwallowed(SwallowAbility swallower)
    {
        //throw new NotImplementedException();
    }

     public override IEnumerator OnSwallow(SwallowAbility swallower)
    {
        //shrink object being swallowed
        Vector2 scale = GetScale();
        canSwallow = false;


        while (scale.sqrMagnitude > 0.25f)
        {
            scale *= (1.0f - 10.0f * Time.deltaTime);
            SetScale(scale);
            yield return null;
        }


        SetActive(false);//disable object

        //adjust player scale based on what is swallowed
        MassSpringSystem ms = swallower.GetMassSpringSystem();

        ms.SetScale((Vector2)ms.GetScale() + sizeModifier);

    }

    public override IEnumerator OnSpit(SwallowAbility swallower, Vector2 spitDir, float delay)
    {
        PlayerFireEffector f = swallower.GetComponentInChildren<PlayerFireEffector>();
        //start flame
        f.StartFlame();
        yield return null;
    }
}