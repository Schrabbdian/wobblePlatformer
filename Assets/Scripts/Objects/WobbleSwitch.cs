﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(MassSpringSystem))]
public class WobbleSwitch : MonoBehaviour
{
    //Events
    public UnityEvent Enabled, Disabled;

    //Switch variables
    private bool active = false;

    //Scale stuff
    private Vector2 originalScale;
    private float originalFrequency;

    //Component References
    private MassSpringSystem massSpring;
    private Collider2D coll;


	// Use this for initialization
	void Start () 
    {
        //Create Events
        if (Enabled == null) { Enabled = new UnityEvent(); }
        if (Disabled == null) { Disabled = new UnityEvent(); }

		//Initialize Component References
        massSpring = GetComponent<MassSpringSystem>();
        coll = GetComponent<Collider2D>();

        //ignore collisions between trigger and self
        massSpring.SetIgnoreCollision(coll, true);

        //save mass srping values
        originalScale = massSpring.GetScale();
        originalFrequency = massSpring.frequency;
	}


    public void OnTriggerStay2D(Collider2D other)
    {
        if (!active)
        {
            float deformation = massSpring.getDeformation();

            if (deformation > 0.25f)
            {
                OnSwitchEnable();

                Enabled.Invoke();
            }
        }
    }


    public void OnSwitchDisable()
    {
        active = false;

        massSpring.setFrequency(originalFrequency);
        massSpring.SetScale(originalScale);

        transform.localScale = new Vector3(massSpring.GetScale().x, massSpring.GetScale().y, transform.localScale.z);

        Disabled.Invoke();
    }

    public void OnSwitchEnable()
    {
        active = true;

        massSpring.setFrequency(originalFrequency * 2.0f);
        massSpring.SetScale(new Vector2(1.25f * originalScale.x, 0.5f * originalScale.y));

        transform.localScale = new Vector3(massSpring.GetScale().x, massSpring.GetScale().y, transform.localScale.z);
    }

}
