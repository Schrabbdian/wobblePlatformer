﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSwallow : Swallowable 
{
    public override void WhileInhaled(SwallowAbility inhaler)
    {

    }

    public override void WhileSwallowed(SwallowAbility swallower)
    {

        Freezable f = GetComponent<Freezable>();
        if(f != null && f.GetFreezeHealth() > 0)
            f.SetFreezeHealth(0);
    }
}