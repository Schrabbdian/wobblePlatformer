﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MassSpringSystem))]
[RequireComponent(typeof(MassSpringCollisionCollector))]
public class Splittable : MonoBehaviour, MassSpringCollectedCollision
{
    //Splitting Parameters
    public MassSpringSystem part;//sub-objects that this object will be split into
   
    [Range(1, 6)]
    public int partCount = 2;
    public float splitThreshold = 0.3f;
    

    //Effects / Visuals
    public GameObject splitEffect;

    //Component References
    private MassSpringSystem massSpring;
    private Swallowable swallowAble;

    //internal variables
    [SerializeField]
    private float deformation = 0.0f;
    [SerializeField]
    private bool collidingWithPointy = false;
    [SerializeField]
    private bool canSplit = true;

	// Initialization
	void Start () 
    {
		//set component references
        massSpring = GetComponent<MassSpringSystem>();
        swallowAble = GetComponent<Swallowable>();
	}


    void FixedUpdate()
    {
        if (swallowAble != null && swallowAble.state != Swallowable.State.Ready) return;//if a swallow script is currently manipulating the object, disable splitting


        
        
        deformation = massSpring.getDeformation();//calculate deformation

        //re-enable splitting upon becoming un-deformed
        if (!canSplit && deformation < 0.05f)
        {
            canSplit = true;
        }

        //if splitting enabled and colliding with pointy object
        if (canSplit && collidingWithPointy)
        {           
            if(deformation > splitThreshold) { OnSplit(); }//split object if deformed beyond threshold
        }

        collidingWithPointy = false;
    }


    //Splitting Methods
    void OnSplit()
    {
        massSpring.SetActive(false);

        //instantiate splitting effect
        Instantiate(splitEffect, transform.position + Vector3.back * 2.0f, Quaternion.identity);

        //Adjust prefab scale for spawning (will be reset afterwards)
        Vector3 prefabScale = part.transform.localScale;
        part.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);

        ////instantiate parts
        MassSpringSystem[] parts = new MassSpringSystem[partCount];
        Vector2 spawnOffset = Vector2.up * 0.25f * massSpring.GetScale().x;

        for (int i = 0; i < partCount; i++)
        {
            parts[i] = (MassSpringSystem)Instantiate(part, transform.position + (Vector3)spawnOffset, Quaternion.identity);

            spawnOffset = Quaternion.AngleAxis(360 / partCount, Vector3.back) * spawnOffset;//rotate offset vector

            parts[i].StartCoroutine(spawnPart(parts[i]));//the coroutine needs to be associated with the spawned gameobject since this one will be inactive / destroyed 
        }       

        part.transform.localScale = prefabScale;//reset prefab scale

        

        Destroy(this.gameObject);
    }

    private IEnumerator spawnPart(MassSpringSystem ms)
    {
        Splittable splittable = ms.GetComponent<Splittable>();
        if (splittable) { splittable.canSplit = false; }

        yield return new WaitForFixedUpdate();

        float partMass = massSpring.mass / partCount;
        Vector2 targetSize = Mathf.Sqrt(1 / (float)partCount) * massSpring.GetScale();//preserve volume (at least for circles)
       
        ms.setFrequency(Mathf.Clamp(ms.frequency * (ms.mass / partMass), 1.0f, 4.0f));//increase frequency with decreasing mass (so small objects dont get totally flattened)
        ms.SetMass(partMass);

        //grow part to target size
        Vector2 scale = ms.GetScale();

        while (scale.x < targetSize.x || scale.y < targetSize.y)
        {
            scale *= (1.0f + 5.0f * Time.deltaTime);
            ms.SetScale(scale);
            yield return null;
        }

    }




    //Point collision methods
    public void OnCollisionStayColl(Collision2D coll,GameObject resolvedObject)
    {
        if (coll.gameObject.CompareTag("Pointy"))
        {
            collidingWithPointy = true;
        }
    }

    public void OnCollisionExitColl(Collision2D coll,GameObject resolvedObject) {}
    public void OnCollisionEnterColl(Collision2D coll,GameObject resolvedObject) {}
}
