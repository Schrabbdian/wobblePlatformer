﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public abstract class Effector2D : MonoBehaviour
{
    public bool Enabled;
    [Range(0.1f, 10)]
    public float Strength;
    public ParticleSystem EffectorParticleSystem;
    private List<GameObject> MSSStayedThisFrame = new List<GameObject>();

    public float GetStrength()
    {
        return Strength;
    }

    public void SetStrength(float strength)
    {
        this.Strength = strength;
    }

	// Use this for initialization
	public void Start () {
	    OnStart();
	}
    
    // Update is called once per frame
    void FixedUpdate()
    {
        MSSStayedThisFrame.Clear();
    }

    protected abstract void OnStart();

    public void OnTriggerStay2D(Collider2D c)
    {
        if (!Enabled)
        {
            return;
        }

        //ignore player
        if (c.gameObject.layer == LayerMask.NameToLayer("Player"))
            return;

        //check if its a mass spring system
        MassPoint mp = c.gameObject.GetComponent<MassPoint>();
        if (mp != null)
        {
            //check if the mass spring system has already entered this trigger (many different masspoints could enter in the same frame)
            GameObject mssGameObject = mp.parent;

            if (MSSStayedThisFrame.Contains(mssGameObject))
            {
                //if this mass spring system has already stayed in this frame ignore all further masspoints
                return;
            }
            else
            {
                //if this mass spring system hasnt been registered this frame, register it so all further collision can be ignored
                MSSStayedThisFrame.Add(mssGameObject);

                Affect(mssGameObject);
            }
        }
        //it is just a rigidbody
        else
        {
            Affect(c.gameObject);
        }
    }

    /// <summary>
    /// Should never be null
    /// </summary>
    /// <param name="g"></param>
    protected abstract void Affect(GameObject g);
}
