﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireEffector : Effector2D
{
    protected override void OnStart()
    {

    }

    //if anything flammable is in effect raduis set it aflame
    protected override void Affect(GameObject g)
    {
        IFlammable f = g.GetComponent<IFlammable>();

        if (f != null)
        {
            f.OnFlame(this);
        }
    }
}
