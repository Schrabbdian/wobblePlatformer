﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFireEffector : FireEffector {

    public float FireDuration;

    private SwallowAbility Swallower;

    // Use this for initialization
    protected override void OnStart () {
        Swallower = GetComponentInParent<SwallowAbility>();
	}
    
    //start flaming
    public void StartFlame()
    {
        EffectorParticleSystem.Play();
        StartCoroutine(Fire(FireDuration));
    }

    private IEnumerator Fire(float duration)
    {
        Enabled = true;
        yield return new WaitForSeconds(duration);

        Enabled = false;
    }

    // Update is called once per frame
    void Update () {
        if (Enabled)
        {
            //rotate object
            Vector2 aimdirection = Swallower.GetCurrentAimDirection();
            float angleToRotate = Vector2Util.signedAngle(gameObject.transform.up, aimdirection);
            gameObject.transform.Rotate(Vector3.forward, angleToRotate);
        }
    }

    //if anything flammable is in effect raduis set it aflame
    protected override void Affect(GameObject g)
    {
        IFlammable f = g.GetComponent<IFlammable>();

        if (f != null)
        {
            f.OnFlame(this);
        }
    }
}
