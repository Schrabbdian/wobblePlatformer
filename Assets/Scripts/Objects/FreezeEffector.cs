﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FreezeEffector : Effector2D
{
    protected override void OnStart()
    {
    }

    protected override void Affect(GameObject g)
    {
        Freezable freezable = g.GetComponent<Freezable>();
        if (freezable != null)
        {
            freezable.OnFreeze(this);
        }
    }
}