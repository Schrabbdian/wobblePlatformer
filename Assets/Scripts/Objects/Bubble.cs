﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MassSpringSystem))]
public class Bubble : Swallowable 
{
    [Header("Player Attributes While Swallowed")]
    public float GravityScaleSwallowed = -0.1f;
    public PlayerMovement.MovementSettings MovementSettingsSwallowed;

    [Header("Bubble Behaviour")]
    public float TimeToLive = 5.0f;//amount in seconds after which the bubble pops
    public bool PopsAfterTime = false;

    [Header("Visuals")]
    public GameObject PopEffect;

    private float preSwallowGravityScale = 1.0f;
    private PlayerMovement.MovementSettings preSwallowedMovementSettings;


    void Update()
    {
        if (TimeToLive > 0.0f)
        {
            TimeToLive -= Time.deltaTime;
        }

        if (PopsAfterTime && TimeToLive <= 0.0f)
        {
            Pop();
        }
 
    }

    //Swallow Methods
    public override void WhileInhaled(SwallowAbility inhaler)
    {
    }

    public override void WhileSwallowed(SwallowAbility swallower)
    {
    }

    public override IEnumerator OnSpit(SwallowAbility swallower, Vector2 spitDir, float delay)
    {
        //Get Swallower Components
        MassSpringSystem ms = swallower.GetMassSpringSystem();
        PlayerMovement playerMove = swallower.GetComponent<PlayerMovement>();

        foreach (Rigidbody2D r in ms.getPointRigidbodies())
        {
            r.gravityScale = preSwallowGravityScale;//restore saved gravity scale
        }

        if (playerMove != null)
        {
            playerMove.moveSettings = preSwallowedMovementSettings;//restore movement settings
        }

        yield return swallower.StartCoroutine(base.OnSpit(swallower, spitDir, delay));
    }

    public override IEnumerator OnSwallow(SwallowAbility swallower)
    {
        yield return StartCoroutine(base.OnSwallow(swallower));

        //Get Swallower Components
        MassSpringSystem ms = swallower.GetMassSpringSystem();
        PlayerMovement playerMove = swallower.GetComponent<PlayerMovement>(); 

        //Alter gravity scale
        preSwallowGravityScale = 0.0f;//save original

        foreach (Rigidbody2D r in ms.getPointRigidbodies())
        {
            preSwallowGravityScale += r.gravityScale;
            r.gravityScale = GravityScaleSwallowed;//set gravity scale to the bubble's value
        }

        preSwallowGravityScale /= ms.getPointRigidbodies().Count;

        //Alter movement settings
        if (playerMove != null) 
        { 
            preSwallowedMovementSettings = playerMove.moveSettings;//save original
            playerMove.moveSettings = MovementSettingsSwallowed;//set it to the bubble's values
        }


    }


    //"Death"
    public void Pop()//the bubble's "Death" method
    {
        StartCoroutine(popAnimation());
        this.enabled = false;
        massSpring.SetCollidersEnabled(false);
    }

    private IEnumerator popAnimation()
    {
        massSpring.setFrequency(massSpring.frequency / 2.0f);
        massSpring.SetScale(massSpring.GetScale() * 2.0f);

        yield return new WaitForSeconds(0.25f);

        if (PopEffect != null)
        {
            Instantiate(PopEffect, transform.position, Quaternion.identity);
        }

        Destroy(this.gameObject);
    }
}
