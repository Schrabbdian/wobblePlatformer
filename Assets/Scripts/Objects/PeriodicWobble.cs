﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MassSpringSystem))]
public class PeriodicWobble : MonoBehaviour 
{
    public float WobbleInterval = 2.0f;
    public float Offset = 0.0f;

    //Component references
    private MassSpringSystem massSpring;

	//Initialization
	void Start () 
    {
        massSpring = GetComponent<MassSpringSystem>();

        StartCoroutine(periodicWobble(WobbleInterval, Offset));
	}



    //Wobble Methods
    private IEnumerator periodicWobble(float interval, float offset)
    {
        yield return new WaitForSeconds(offset);

        while (true)
        {
            StartCoroutine(shortWobble());

            yield return new WaitForSeconds(interval);           
        }
    }

    private IEnumerator shortWobble()
    {
        Vector2 originalScale = massSpring.GetScale();

        massSpring.SetScale(new Vector2(originalScale.x * 1.25f, originalScale.y * 2.0f));

        yield return new WaitForSeconds(0.1f);

        massSpring.SetScale(new Vector2(massSpring.GetScale().x / 1.25f, massSpring.GetScale().y / 2.0f));
    }
}
