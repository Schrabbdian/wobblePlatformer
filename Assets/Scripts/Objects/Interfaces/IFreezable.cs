﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// When an object is frozen by the IceBlock script all components that implement freezeable are interfaced with
/// </summary>
public interface IFreezable
{
    /// <summary>
    /// Called when the object is frozen
    /// </summary>
    void OnFreeze();

    /// <summary>
    /// Called when the object is unfrozen
    /// </summary>
    void OnUnfreeze();

    /// <summary>
    /// Called when the objects freeze strength changes
    /// </summary>
    /// <param name="current">Current Freeze Strength</param>
    /// <param name="max">Maximum Freeze Strength</param>
    /// <param name="change">Strength Change this frame</param>
    void OnFreezeChanged(float current, float max, float change);
}
