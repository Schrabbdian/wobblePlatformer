﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//todo if all masspoints are already not affected by the block also totallyunfreeze
public class Freezable : MonoBehaviour
{
    public enum FreezeStatus
    {
        NotFrozenYet,
        Frozen,
        Unfrozen
    }

    private enum Mode
    {
        MassSpring,
        Rigidbody
    }

    //Editor Stuff
    public bool FrozenOnStart;
    [Range(0.001f, 1f)]
    public float FreezeThreshold = 0.5f; 
    public float MaximumFreezeHealth = 2f;
    public Gradient FrozenColorGradient;

    //GameObject References
    public GameObject IceBlockPrefab;
    
    //Component References
    private IceBlock iceBlock;
    private MassSpringSystem massSpring;
    private Rigidbody2D myRigidBody;
    private List<IFreezable> freezeScripts = new List<IFreezable>();

    //Ice Block internal parameters
    private Mode mode;
    private FreezeStatus Status;

    // Use this for initialization
    void Start()
    {
        //Set Up Component References
        massSpring = gameObject.GetComponent<MassSpringSystem>();
        myRigidBody = gameObject.GetComponent<Rigidbody2D>();

        if (massSpring != null)
        {
            mode = Mode.MassSpring;
            
        }
        else
        {
            mode = Mode.Rigidbody;
        }

    }

    public void Update()
    {
        //happens in first frame because massspringsystems change the parent of masspoint in start
        if (FrozenOnStart)
        {
            SetFreezeHealth(MaximumFreezeHealth);
            FrozenOnStart = false;
        }
    }

    public void FixedUpdate()
    {
        if (mode == Mode.MassSpring)
        {
            switch (GetStatus())
            {
                case FreezeStatus.Frozen:
                    CheckFrozenPoints();
                    CheckUnfrozenPoints();
                    break;

                case FreezeStatus.NotFrozenYet:
                    CheckFrozenPoints();
                    CheckUnfrozenPoints();
                    break;

            }
        }

    }

    private void OnSpawn()
    {
        CreateIceBlock();
        iceBlock.transform.SetParent(transform, true);
    }

    private void OnDespawn()
    {
        DestroyIceBlock();
    }

    //called the moment it gets fully frozen
    private void OnFullyFrozen()
    {
        switch (mode)
        {
            case Mode.MassSpring:
                //child freezable to prefab
                iceBlock.transform.SetParent(null, true);
                foreach (Transform t in massSpring.MassPoints)
                {
                    t.SetParent(iceBlock.transform, true);
                }

                //move velocity to iceblock
                iceBlock.SetVelocities(GetVelocity(), GetAngularVelocity());

                //disable physics of frozen object but not collision
                foreach (Rigidbody2D r in massSpring.getPointRigidbodies())
                {
                    r.bodyType = RigidbodyType2D.Kinematic;

                    //set velocity to 0 it doesnt move inside the iceblock after it is frozen
                    r.velocity = Vector2.zero;
                    r.angularVelocity = 0;
                }
                

                break;

            case Mode.Rigidbody:
                //child freezable to prefab
                iceBlock.transform.SetParent(null, true);
                transform.SetParent(iceBlock.transform);

                //disable physics of frozen object but not collision
                myRigidBody.bodyType = RigidbodyType2D.Kinematic;

                //move velocity to iceblock
                iceBlock.SetVelocities(GetVelocity(), GetAngularVelocity());
                myRigidBody.velocity = Vector2.zero;
                myRigidBody.angularVelocity = 0;

                break;
        }


        //freeze all freezescripts
        freezeScripts.AddRange(gameObject.GetComponents<IFreezable>());
        foreach (IFreezable f in freezeScripts)
        {
            f.OnFreeze();
        }
        
        Debug.Log("Frozen: " + gameObject.name);
    }

    //called when the object definitely becomes unfrozen
    private void OnFullyUnfrozen()
    {
        switch (mode)
        {
            case Mode.MassSpring:
                //unfreeze all points
                foreach (Rigidbody2D r in massSpring.getPointRigidbodies())
                {
                    //unparent
                    r.transform.SetParent(null, true);

                    //set the rigidbody to dynamic
                    r.bodyType = RigidbodyType2D.Dynamic;
                }

                break;

            case Mode.Rigidbody:
                //unchild freezable
                transform.SetParent(null, true);

                //enable physics on rigidbody
                myRigidBody.bodyType = RigidbodyType2D.Dynamic;//should technically keep velocity and rotation
                
                
                break;
        }

        //unfreeze all scripts
        foreach (IFreezable f in freezeScripts)
        {
            f.OnUnfreeze();
        }

        //reparent gameobject
        iceBlock.transform.SetParent(transform, true);

        /*DestroyIceBlock();*/
        Debug.Log("Unfrozen: " + gameObject.name);
    }

    public void OnFreeze(FreezeEffector effector)
    {
        float freezeChange = Time.fixedDeltaTime * effector.GetStrength();
        SetFreezeHealth(GetFreezeHealth() + freezeChange);

        OnFreezeChanged(freezeChange);
    }

    public void OnFreezeChanged(float delta)
    {
        if(delta == 0)
            return;

        foreach (IFreezable freezeScript in freezeScripts)
        {
            freezeScript.OnFreezeChanged(GetFreezeHealth(), MaximumFreezeHealth, delta);
        }
    }

    private void CheckUnfrozenPoints()
    {
        foreach (Rigidbody2D r in massSpring.getPointRigidbodies())
        {
            //if a masspoint is not in the iceblock anymore
            if (!iceBlock.IsInside(r.position))
            {
                //unparent this point
                r.transform.SetParent(null, true);

                //set the rigidbody to dynamic
                r.bodyType = RigidbodyType2D.Dynamic;
            }
        }
    }

    private void CheckFrozenPoints()
    {
        //if it is inside reparent
        foreach (Rigidbody2D r in massSpring.getPointRigidbodies())
        {
            //if a masspoint is not in the iceblock anymore
            if (iceBlock.IsInside(r.position))
            {
                //unparent this point
                r.transform.SetParent(iceBlock.transform, true);

                //set the rigidbody to dynamic
                r.bodyType = RigidbodyType2D.Kinematic;
                r.velocity = Vector2.zero;
                r.angularVelocity = 0;
            }
        }
    }

    public void SetFreezeHealth(float value)
    {
        if (value > 0 && iceBlock == null)
        {
            OnSpawn();
        }

        float currentHealth = GetFreezeHealth();
        iceBlock.SetHealth(value);
        
        DebugFreezeHealth = GetFreezeHealth();

        //we just transitioned
        if (currentHealth < FreezeThreshold * MaximumFreezeHealth && GetFreezeHealth() >= FreezeThreshold * MaximumFreezeHealth)
        {
            OnFullyFrozen();    
        }
        else if(currentHealth >= FreezeThreshold * MaximumFreezeHealth && GetFreezeHealth() < FreezeThreshold * MaximumFreezeHealth)
        {
            OnFullyUnfrozen();
        }

        if (GetFreezeHealth() <= 0)
        {
            OnDespawn();
        }
    }

    //only for debugging purposes
    public float DebugFreezeHealth;

    public float GetFreezeHealth()
    {
        if (iceBlock == null)
        {
            return 0;
        }
        return iceBlock.GetHealth();
    }

    public Vector2 GetVelocity()
    {
        if (mode == Mode.MassSpring)
        {
            return massSpring.getVelocity();
        }
        else if (mode == Mode.Rigidbody)
        {
            return myRigidBody.velocity;
        }
        else
        {
            return Vector2.zero;
        }
    }

    public float GetAngularVelocity()
    {
        if (mode == Mode.MassSpring)
        {
            return 0;
        }
        else if(mode == Mode.Rigidbody)
        {
            return myRigidBody.angularVelocity;
        }
        else
        {
            return 0;
        }
    }

    public void SetVelocities(Vector2 velocity, float angularVelocity)
    {
        if (mode == Mode.MassSpring)
        {
            massSpring.setVelocity(velocity);
            //todo set angular velocity of massspringsystem
        }
        else if (mode == Mode.Rigidbody)
        {
            myRigidBody.velocity = velocity;
            myRigidBody.angularVelocity = angularVelocity;
        }
    }

    private FreezeStatus GetStatus()
    {
        if (iceBlock == null)
        {
            return FreezeStatus.Unfrozen;
        }
        else
        {
            return iceBlock.GetStatus();
        }
    }

    private void CreateIceBlock()
    {
        //child iceblock to this
        GameObject iceGo = Instantiate(IceBlockPrefab, transform.position, transform.rotation);
        //iceGo.transform.localPosition = Vector2.zero;
        iceBlock = iceGo.GetComponent<IceBlock>();

        //Set up iceblock
        iceBlock.StartsFrozen = FrozenOnStart;
        iceBlock.FrozenGameObject = this;
        iceBlock.FreezeThreshold = FreezeThreshold;
        iceBlock.MaxHealth = MaximumFreezeHealth;
        iceBlock.FrozenColorGradient = FrozenColorGradient;
        iceBlock.StartScale = iceBlock.GetVisual().transform.localScale;
        Collider2D iceBlockCollider = iceBlock.GetCollider();

        //add mass, velocities and setup ignored colliders
        if (mode == Mode.MassSpring)
        {
            iceBlock.MassToAdd = massSpring.mass;
            massSpring.SetIgnoreCollision(iceBlockCollider, true);
        }
        else
        {
            iceBlock.MassToAdd = myRigidBody.mass;
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), iceBlockCollider);
        }
    }

    private void DestroyIceBlock()
    {
        iceBlock.transform.SetParent(null);
        Destroy(iceBlock.gameObject);

        iceBlock = null;
    }
}