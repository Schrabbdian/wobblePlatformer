﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceBlock : MonoBehaviour, IFlammable
{
    private static float minScale = 0.5f;

    //Freeze Health
    [HideInInspector] public float FreezeThreshold; //is set from freeze

    [HideInInspector] public float MaxHealth; //is set from freezable script
    [HideInInspector] public bool StartsFrozen = false; //is set from freezable script
    private float health;
    private Freezable.FreezeStatus Status;

    //GameObject References
    [HideInInspector] public Freezable FrozenGameObject; //is set from freezable script

    private GameObject Visual;

    //Component References
    private Rigidbody2D myRigidbody;

    private Collider2D myCollider;
    private Material myMaterial;

    //Other Variables
    [HideInInspector] public float MassToAdd; //is set from freezable script

    [HideInInspector] public Gradient FrozenColorGradient;

    //visual/animation
    [HideInInspector] public Vector3 StartScale; //is set from freezable script

    #region Components

    public GameObject GetVisual()
    {
        if (Visual == null)
        {
            Visual = transform.GetChild(0).gameObject; //the visual part of the iceblock should always be the first child
        }
        return Visual;
    }

    private Rigidbody2D GetRigidBody()
    {
        if (myRigidbody == null)
        {
            myRigidbody = GetComponent<Rigidbody2D>();
        }
        return myRigidbody;
    }

    public Collider2D GetCollider()
    {
        if (myCollider == null)
        {
            myCollider = GetVisual().GetComponent<Collider2D>();
        }
        return myCollider;
    }

    #endregion

    public void Start()
    {
        //Set up gameobject references
        GetVisual();

        //Set up component references
        GetRigidBody();
        GetCollider();
        myMaterial = GetVisual().GetComponent<Renderer>().material;

        //setup start scale
        //StartScale = GetVisual().transform.localScale;

        //change behaviour depending if it should start frozen
        if (StartsFrozen)
        {
            //Set health from 0 to something above 0 so onSpawn is called
            health = 0;
            OnSpawn();
            //then set the health to maximum to freeze it
            SetHealth(MaxHealth);
        }
        else
        {
            //Set health from 0 to something above 0 so onSpawn is called
            health = 0;
            SetHealth(0.01f);
        }
    }

    public void FixedUpdate()
    {
        if (FrozenGameObject != null)
        {
            //adjust scale
            GetVisual().transform.localScale = Vector3.Lerp(minScale * StartScale, StartScale, GetFreezePercentage());

            //change color
            myMaterial.color = FrozenColorGradient.Evaluate(GetFreezePercentage());
        }
    }

    public void OnFlame(FireEffector effector)
    {
        //cannot be affected by flames if not frozen
        switch (GetStatus())
        {
            case Freezable.FreezeStatus.Unfrozen:
                return;
        }

        float freezeChange = -Time.fixedDeltaTime * effector.GetStrength();
        FrozenGameObject.SetFreezeHealth(GetHealth() + freezeChange);

        FrozenGameObject.OnFreezeChanged(freezeChange);
    }

    #region Speed

    public Vector2 GetVelocity()
    {
        return GetRigidBody().velocity;
    }

    public float GetAngularVelocity()
    {
        return GetRigidBody().angularVelocity;
    }

    public void SetVelocities(Vector2 velocity, float angularVelocity)
    {
        GetRigidBody().velocity = velocity;
        GetRigidBody().angularVelocity = angularVelocity;
    }

    #endregion

    #region Health

    public void SetHealth(float health)
    {
        float previousHealth = this.health;
        this.health = Mathf.Clamp(health, 0, MaxHealth);

        //health hasnt changed
        if ((health - previousHealth) == 0)
        {
            return;
        }

        //health is below 0
        if (health <= 0)
        {
            this.health = 0;
            OnDespawn();
        }
        //health has just changed from 0 to something positive
        else if (previousHealth <= 0 && health > 0)
        {
            OnSpawn();
        }

        //health just changed above freezethreshold
        if (previousHealth < FreezeThreshold * MaxHealth && health >= FreezeThreshold * MaxHealth)
        {
            OnFullyFrozen();
        }
        //health just change below freezethreshold
        else if (previousHealth >= FreezeThreshold * MaxHealth && health < FreezeThreshold * MaxHealth)
        {
            OnFullyUnfrozen();
        }

        //animate here
    }

    public float GetHealth()
    {
        return health;
    }

    #endregion

    public Freezable.FreezeStatus GetStatus()
    {
        return Status;
    }

    //returns if a position is inside the boxcollider
    public bool IsInside(Vector2 position)
    {
        return GetCollider().OverlapPoint(position);
    }

    private void OnSpawn()
    {
        //rigidbody and collider disabled so the iceblock is only visual
        GetRigidBody().bodyType = RigidbodyType2D.Kinematic;
        GetCollider().enabled = false;

        //add mass from frozen gameobject to the iceblock
        GetRigidBody().mass += MassToAdd;

        GetVisual().transform.localScale = Vector3.Lerp(minScale * StartScale, StartScale, GetFreezePercentage());

        Status = Freezable.FreezeStatus.NotFrozenYet;
    }

    private void OnDespawn()
    {
        Status = Freezable.FreezeStatus.Unfrozen;
    }

    private void OnFullyFrozen()
    {
        //enable physics and collision
        GetRigidBody().bodyType = RigidbodyType2D.Dynamic;
        GetCollider().enabled = true;

        Status = Freezable.FreezeStatus.Frozen;
    }

    private void OnFullyUnfrozen()
    {
        GetRigidBody().bodyType = RigidbodyType2D.Kinematic;
        GetCollider().enabled = false;

        //move Velocity to freezable
        FrozenGameObject.SetVelocities(GetVelocity(), GetAngularVelocity());

        GetRigidBody().velocity = Vector2.zero;
        GetRigidBody().angularVelocity = 0;

        Status = Freezable.FreezeStatus.NotFrozenYet;
    }

    private float GetFreezePercentage()
    {
        return health / MaxHealth;
    }
}