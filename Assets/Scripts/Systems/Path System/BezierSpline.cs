﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Author: Robert, using a Catlikecoding.com tutorial

public enum BezierControlPointMode
{
    Free,
    Aligned,
    Mirrored
}

public class BezierSpline : MonoBehaviour 
{
    [SerializeField]
    private List<Vector3> points = new List<Vector3>(4);

    [SerializeField]
    private List<BezierControlPointMode> modes = new List<BezierControlPointMode>(2);

    [SerializeField]
    private bool loop = false;

    //Gets the point on the spline given "progress / time" t
    public Vector3 GetPoint(float t)
    {
        int curveIndex = 0;//index in points at which the curve starts

        if (t >= 1.0f)
        {
            t = 1.0f;
            curveIndex = points.Count - 4;
        }
        else
        {
            t = Mathf.Clamp01(t) * CurveCount;//t across the whole spline
            curveIndex = Mathf.FloorToInt(t);
            t -= curveIndex;//reduce t again to the fraction along that curve
            curveIndex *= 3;
        }

        return transform.TransformPoint(Bezier.GetPoint(points[curveIndex], points[curveIndex + 1], points[curveIndex + 2], points[curveIndex + 3], t));
    }

    //Gets velocity at that point
    public Vector3 GetVelocity(float t)
    {
        int curveIndex = 0;//index in points at which the curve starts

        if (t >= 1.0f)
        {
            t = 1.0f;
            curveIndex = points.Count - 4;
        }
        else
        {
            t = Mathf.Clamp01(t) * CurveCount;//t across the whole spline
            curveIndex = Mathf.FloorToInt(t);
            t -= curveIndex;//reduce t again to the fraction along that curve
            curveIndex *= 3;
        }

        return transform.TransformPoint(Bezier.GetFirstDerivative(points[curveIndex], points[curveIndex + 1], points[curveIndex + 2], points[curveIndex + 3], t)) - transform.position;
    }

    public void Reset()//used by unityeditor to init / rest
    {
        points.Clear();

        points.Add(new Vector3(0f, 0f, 0f));
        points.Add(new Vector3(1f, 0f, 0f));
        points.Add(new Vector3(2f, 0f, 0f));
        points.Add(new Vector3(3f, 0f, 0f));

        modes.Clear();
        modes.Add(BezierControlPointMode.Free);
        modes.Add(BezierControlPointMode.Free);
    }

    public void AddCurve()
    {
        Vector3 lastPoint = points[points.Count - 1];

        //add three new points 
        //(a new Bezier Curve consists of 4 points where the first point equals the last of the previous curve in the spline)
        points.Add(new Vector3(lastPoint.x + 1.0f, lastPoint.y, lastPoint.z));
        points.Add(new Vector3(lastPoint.x + 2.0f, lastPoint.y, lastPoint.z));
        points.Add(new Vector3(lastPoint.x + 3.0f, lastPoint.y, lastPoint.z));

        //add a new mode, since there is a new intersection between 2 curves
        modes.Add(modes[modes.Count - 1]);

        EnforceMode(points.Count - 4);//enforce mode at the transition point

        if (loop)
        {
            //ensure new last point is connected to first
            points[points.Count - 1] = points[0];
            modes[modes.Count - 1] = modes[0];
            EnforceMode(0);
        }
    }

    // given an index this tries to remove the point
    // this is only possible if the curve would not vanish
    // by doing this
    // also only position points can be removed
    public void RemoveCurve(int index)
    {
        // check for valid index
        if (index < 0 || index >= points.Count || CurveCount <= 1 || index % 3 != 0)
            return;

        // if first to remove, remove first three elements
        if (index == 0)
        {
            points.RemoveAt(0);
            points.RemoveAt(0);
            points.RemoveAt(0);
        }
        // if in the middle to remove
        // remove the three elements on index-1, index and index+1
        else if (index < points.Count-1)
        {
            points.RemoveAt(index-1);
            points.RemoveAt(index-1);
            points.RemoveAt(index-1);
        }
        // if the last to remove
        // remove the last 3 points
        else if (index == points.Count-1)
        {
            points.RemoveAt(index-2);
            points.RemoveAt(index-2);
            points.RemoveAt(index-2);
        }

        // fix the curve again
        if (loop)
        {
            //ensure new last point is connected to first
            points[points.Count - 1] = points[0];
            modes[modes.Count - 1] = modes[0];
            EnforceMode(0);
        }

        for (int i = 1; i < points.Count / 3; i++)
        {
            EnforceMode(i * 3);
        }

    }

    public int CurveCount//how many curves make up the spline?
    {
        get 
        {
            return points.Count / 3;
        }
    }


    //public access
    public int ControlPointCount
    {
        get
        {
            return points.Count;
        }
    }

    public bool Loop
    {
        get
        {
            return loop;
        }
        set
        {
            loop = value;
            if (value)
            {
                //connect first and last point
                modes[modes.Count - 1] = modes[0];//same mode
                SetControlPoint(0, points[0]);//same position
            }
        }
    }


    public Vector3 GetControlPoint(int index)
    {
        return points[index];
    }

    public void SetControlPoint(int index, Vector3 point)
    {
        if (index % 3 == 0)//if we are manipulating a non-tangenthandle-point
        {
            Vector3 delta = point - points[index];
            
            //move adjacent points (corresponding tangent-handles) with it
            if (loop)
            {
                //special case for first and last point since they wrap around the array
                if (index == 0)//first point
                {
                    points[1] += delta;
                    points[points.Count - 2] += delta;
                    points[points.Count - 1] = point;
                }
                else if (index == points.Count - 1)//last point
                {
                    points[0] = point;
                    points[1] += delta;
                    points[index - 1] += delta;
                }
                else
                {
                    points[index - 1] += delta;
                    points[index + 1] += delta;
                }
            }
            else
            {

                if (index > 0)
                {
                    points[index - 1] += delta;
                }
                if (index + 1 < points.Count)
                {
                    points[index + 1] += delta;
                }
            }
        }

        points[index] = point;
        EnforceMode(index);
    }

    public BezierControlPointMode GetControlPointMode(int index)
    {
        return modes[(index + 1) / 3];
    }

    public void SetControlPointMode(int index, BezierControlPointMode mode)
    {
        int modeIndex = (index + 1) / 3;
        modes[modeIndex] = mode;

        if (loop)
        {
            //ensure first and last points have the same mode
            if (modeIndex == 0)
            {
                modes[modes.Count - 1] = mode;
            }
            else if (modeIndex == modes.Count - 1)
            {
                modes[0] = mode;
            }
        }

        EnforceMode(index);
    }

    //Control Point Mode
    private void EnforceMode(int index)
    {
        //check whether mode has to be enforced
        int modeIndex = (index + 1) / 3;
        BezierControlPointMode mode = modes[modeIndex];
        if (mode == BezierControlPointMode.Free || !loop && (modeIndex == 0 || modeIndex == modes.Count - 1))
        {
            //either the point is not between to curves or it is set to free
            return;
        }

        //determine which point is fixed and which needs to be enforced
        int middleIndex = modeIndex * 3;
        int fixedIndex, enforcedIndex;
        if (index <= middleIndex)
        {
            fixedIndex = middleIndex - 1;
            if (fixedIndex < 0)
            {
                fixedIndex = points.Count - 2;
            }
            enforcedIndex = middleIndex + 1;
            if (enforcedIndex >= points.Count)
            {
                enforcedIndex = 1;
            }
        }
        else
        {
            fixedIndex = middleIndex + 1;
            if (fixedIndex >= points.Count)
            {
                fixedIndex = 1;
            }
            enforcedIndex = middleIndex - 1;
            if (enforcedIndex < 0)
            {
                enforcedIndex = points.Count - 2;
            }
        }

        Vector3 middle = points[middleIndex];
        Vector3 enforcedTangent = middle - points[fixedIndex];

        if (mode == BezierControlPointMode.Aligned)
        {
            //for aligned, keep original distance
            enforcedTangent = enforcedTangent.normalized * Vector3.Distance(middle, points[enforcedIndex]);
        }

        points[enforcedIndex] = middle + enforcedTangent;//enforce movement to be mirrored
    }
}
