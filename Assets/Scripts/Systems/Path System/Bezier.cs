﻿using UnityEngine;
using System.Collections;

//Author: Robert, using a Catlikecoding.com tutorial

public static class Bezier 
{

    //returns point along a four-point bezier curve given "progress / time" t
    public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = Mathf.Clamp01(t);//ensure t is in [0, 1]
        float oneMinusT = 1f - t;
        
        return
            oneMinusT * oneMinusT * oneMinusT * p0 +
            3f * oneMinusT * oneMinusT * t * p1 +
            3f * oneMinusT * t * t * p2 +
            t * t * t * p3;
 
    }

    public static Vector3 GetFirstDerivative(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = Mathf.Clamp01(t);//ensure t is in [0, 1]
        float oneMinusT = 1f - t;

        return
            3f * oneMinusT * oneMinusT * (p1 - p0) +
            6f * oneMinusT * t * (p2 - p1) +
            3f * t * t * (p3 - p2);
    }

}
