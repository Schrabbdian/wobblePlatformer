﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class LineRenderer2D : MonoBehaviour 
{

    public Vector3[] Positions;
    public float lineWidth = 0.5f;

    //public bool hasEndCap = false;
    [Range(0, 1.0f)]
    public float endCapLength = 0.25f;

    //component references
    private MeshRenderer meshRenderer;
    private MeshFilter meshFilter;

	//MonoBehaviour Events:
	void Start () 
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshFilter = GetComponent<MeshFilter>();

        //create mesh
        Mesh lineMesh = new Mesh();
        meshFilter.mesh = lineMesh;

	}
	
	void Update () 
    {
        recalculateMesh();
	}

    




    private void recalculateMesh()
    {
        if (Positions.Length < 2) return;//can only draw a line between at least 2 points

        Mesh lineMesh = meshFilter.mesh;
        lineMesh.Clear();

        Vector3[] vertices = new Vector3[2 * Positions.Length];

        Vector2[] uvs = new Vector2[vertices.Length];

        int triangleCount = 2 * (Positions.Length - 1);//every position needs 2 triangles, except for start and end which require 1 each
        int[] triangles = new int[3 * triangleCount];


        lineMesh.subMeshCount = meshRenderer.materials.Length;//one submesh for every material
        //NOTE: Materials must have the same transparency mode (e.g. 'Fade')


        //VERTICES:

        //start and end positions
        Vector3 toNext = Positions[1] - Positions[0];
        vertices[0] = Positions[0] - new Vector3(toNext.y, -toNext.x, 0).normalized * lineWidth;
        vertices[1] = Positions[0] + new Vector3(toNext.y, -toNext.x, 0).normalized * lineWidth;

        Vector3 toPrev = Positions[Positions.Length - 2] - Positions[Positions.Length - 1];
        vertices[vertices.Length - 1] = Positions[Positions.Length - 1] - new Vector3(toPrev.y, -toPrev.x, 0).normalized * lineWidth;
        vertices[vertices.Length - 2] = Positions[Positions.Length - 1] + new Vector3(toPrev.y, -toPrev.x, 0).normalized * lineWidth;

        //create two vertices for every position
        for (int i = 1; i < Positions.Length - 1; i++)
        {
            toNext = Positions[i+1] - Positions[i];

            vertices[2 * i] = Positions[i] - new Vector3(toNext.y, -toNext.x, 0).normalized * lineWidth;
            vertices[2 * i + 1] = Positions[i] + new Vector3(toNext.y, -toNext.x, 0).normalized * lineWidth;
        }

        
        //UVs:
        float uvStep = 1.0f / (float)Positions.Length;

        for (int i = 0; i < uvs.Length; i+=2)
        {
            uvs[i] = new Vector2(0, i * uvStep);
            uvs[i + 1] = new Vector2(1, i * uvStep);
        }



        //TRIANGLES:

        //start and end triangles       
        triangles[0] = 0;
        triangles[1] = 3;
        triangles[2] = 1;
        
        triangles[triangles.Length - 1] = vertices.Length - 4;
        triangles[triangles.Length - 2] = vertices.Length - 1;
        triangles[triangles.Length - 3] = vertices.Length - 2;

        
        //2 triangles for every position in between
        int triangleIndex = 1;

        for (int i = 1; i < Positions.Length - 1; i++)
        {

            triangles[3 * triangleIndex] = 2 * i;
            triangles[3 * triangleIndex + 1] = 2 * i + 1;
            triangles[3 * triangleIndex + 2] = 2 * i - 2;

            triangleIndex++; 

            triangles[3 * triangleIndex] = 2 * i;
            triangles[3 * triangleIndex + 1] = 2 * i + 3;
            triangles[3 * triangleIndex + 2] = 2 * i + 1;

            triangleIndex++;       
        }




        //assign new vertices/uvs/triangles
        lineMesh.vertices = vertices;
        lineMesh.uv = uvs;

        if (lineMesh.subMeshCount == 1)//assign all triangles to the same submesh (= material)
        {
            lineMesh.triangles = triangles;
        }
        else //assign triangles to two different materials (1: body, 2: end cap)
        {
            int endCapTriangleCount = 2 * (int)(Positions.Length * endCapLength);

            List<int> trianglesSubMesh1 = new List<int>();
            List<int> trianglesSubMesh2 = new List<int>();

            //split triangle list
            for (int i = 0; i < triangles.Length; i++ )
            {
                if (i < triangles.Length - 3 * endCapTriangleCount)
                {
                    trianglesSubMesh1.Add(triangles[i]);
                }
                else
                {
                    trianglesSubMesh2.Add(triangles[i]);
                }
            }

            lineMesh.SetTriangles(trianglesSubMesh1, 0);
            lineMesh.SetTriangles(trianglesSubMesh2, 1);
        } 

        lineMesh.RecalculateNormals();//important for proper lighting
    }
}
