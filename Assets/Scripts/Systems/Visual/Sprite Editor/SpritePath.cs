﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(BezierSpline))]
[RequireComponent(typeof(MeshFilter))]
public class SpritePath : MonoBehaviour 
{
    //public parameters
    public int resolution = 10;
    public float lineWidth = 0.5f;
    public bool flipX = false;

    //connecting to other SpritePaths
    public SpritePath connectStartTo;
    public bool startToStart;
    public SpritePath connectEndTo;
    public bool endToStart;


    //component references
    private BezierSpline spline;
    private MeshFilter meshFilter;


    //line Mesh
    private Vector3[] linePositions;//positions that make up the line (local space)

    //MonoBehaviour Events:
    void Start()
    {
        //set up component references
        spline = GetComponent<BezierSpline>();
        meshFilter = GetComponent<MeshFilter>();

        //create mesh
        Mesh lineMesh = new Mesh();
        meshFilter.mesh = lineMesh;
    }

    void Update()
    {
        sampleCurve();
        recalculateMesh();
    }

    void OnDrawGizmos()
    {
        /*
        foreach (Vector3 p in linePositions)
        {
            Gizmos.DrawWireSphere(transform.TransformPoint(p), 0.1f);
        }*/
    }


    //Line Mesh Construction
    private void sampleCurve()
    {
        int pointCount = resolution;
        Vector3[] positions = new Vector3[pointCount];//array to store positions along curve

        float sampleInterval = 1.0f / (float)(resolution - 1);

        //sample curve at intervals
        for (int i = 0; i < resolution; i++)
        {
            positions[i] = transform.InverseTransformPoint(spline.GetPoint(i * sampleInterval));
        }


        linePositions = positions;
    }

    private void recalculateMesh()
    {
        if (linePositions.Length < 2) return;//can only draw a line between at least 2 points

        Mesh lineMesh = meshFilter.sharedMesh;
        lineMesh.Clear();

        Vector3[] vertices = new Vector3[2 * linePositions.Length];

        Vector2[] uvs = new Vector2[vertices.Length];

        int triangleCount = 2 * (linePositions.Length - 1);//every position needs 2 triangles, except for start and end which require 1 each
        int[] triangles = new int[3 * triangleCount];


        //lineMesh.subMeshCount = meshRenderer.sharedMaterials.Length;//one submesh for every material
        //NOTE: Materials must have the same transparency mode (e.g. 'Fade')


        //VERTICES:

        //start and end positions
        Vector3 toNext = linePositions[1] - linePositions[0];
        vertices[0] = linePositions[0] - new Vector3(toNext.y, -toNext.x, 0).normalized * lineWidth;
        vertices[1] = linePositions[0] + new Vector3(toNext.y, -toNext.x, 0).normalized * lineWidth;

        Vector3 toPrev = linePositions[linePositions.Length - 2] - linePositions[linePositions.Length - 1];
        vertices[vertices.Length - 1] = linePositions[linePositions.Length - 1] - new Vector3(toPrev.y, -toPrev.x, 0).normalized * lineWidth;
        vertices[vertices.Length - 2] = linePositions[linePositions.Length - 1] + new Vector3(toPrev.y, -toPrev.x, 0).normalized * lineWidth;

        //create two vertices for every position
        for (int i = 1; i < linePositions.Length - 1; i++)
        {
            toNext = linePositions[i + 1] - linePositions[i];

            vertices[2 * i] = linePositions[i] - new Vector3(toNext.y, -toNext.x, 0).normalized * lineWidth;
            vertices[2 * i + 1] = linePositions[i] + new Vector3(toNext.y, -toNext.x, 0).normalized * lineWidth;
        }


        //UVs:
        float uvStep = 1.0f / (float)linePositions.Length;

        for (int i = 0; i < uvs.Length; i += 2)
        {
            if (flipX)
            {
                uvs[i] = new Vector2(i * uvStep, 1);
                uvs[i + 1] = new Vector2(i * uvStep, 0);
            }
            else
            {
                uvs[i] = new Vector2(i * uvStep, 0);
                uvs[i + 1] = new Vector2(i * uvStep, 1);
            }

        }



        //TRIANGLES:

        //start and end triangles       
        triangles[0] = 0;
        triangles[1] = 3;
        triangles[2] = 1;

        triangles[triangles.Length - 1] = vertices.Length - 4;
        triangles[triangles.Length - 2] = vertices.Length - 1;
        triangles[triangles.Length - 3] = vertices.Length - 2;


        //2 triangles for every position in between
        int triangleIndex = 1;

        for (int i = 1; i < linePositions.Length - 1; i++)
        {

            triangles[3 * triangleIndex] = 2 * i;
            triangles[3 * triangleIndex + 1] = 2 * i + 1;
            triangles[3 * triangleIndex + 2] = 2 * i - 2;

            triangleIndex++;

            triangles[3 * triangleIndex] = 2 * i;
            triangles[3 * triangleIndex + 1] = 2 * i + 3;
            triangles[3 * triangleIndex + 2] = 2 * i + 1;

            triangleIndex++;
        }


        //connect to other SpritePaths:

        //connect path start if set
        if (connectStartTo != null)
        {
            if (startToStart)
            {
                //connect vertices
                Vector3 other_vert0 = connectStartTo.getVertexWorldPos(1);
                Vector3 other_vert1 = connectStartTo.getVertexWorldPos(0);

                vertices[0] = transform.InverseTransformPoint(other_vert0);
                vertices[1] = transform.InverseTransformPoint(other_vert1);

                //connect splines
                BezierSpline other = connectStartTo.getSpline();

                Vector3 other_start = other.transform.TransformPoint(other.GetControlPoint(0));
                spline.SetControlPoint(0, transform.InverseTransformPoint(other_start));
            }
            else
            {
                //connect vertices
                int other_vertCount = connectStartTo.getVertexCount();
                Vector3 other_vert0 = connectStartTo.getVertexWorldPos(other_vertCount - 2);
                Vector3 other_vert1 = connectStartTo.getVertexWorldPos(other_vertCount - 1);

                vertices[0] = transform.InverseTransformPoint(other_vert0);
                vertices[1] = transform.InverseTransformPoint(other_vert1);

                //connect splines
                BezierSpline other = connectStartTo.getSpline();

                Vector3 other_end = other.transform.TransformPoint(other.GetControlPoint(other.ControlPointCount - 1));
                spline.SetControlPoint(0, transform.InverseTransformPoint(other_end));
            }
        }

        //connect path end if set
        if (connectEndTo != null)
        {
            if (endToStart)
            {
                //connect vertices
                Vector3 other_vert0 = connectEndTo.getVertexWorldPos(1);
                Vector3 other_vert1 = connectEndTo.getVertexWorldPos(0);

                vertices[vertices.Length - 1] = transform.InverseTransformPoint(other_vert0);
                vertices[vertices.Length - 2] = transform.InverseTransformPoint(other_vert1);

                //connect splines
                BezierSpline other = connectEndTo.getSpline();

                Vector3 other_start = other.transform.TransformPoint(other.GetControlPoint(0));
                spline.SetControlPoint(spline.ControlPointCount - 1, transform.InverseTransformPoint(other_start));
            }
            else
            {
                //connect vertices
                int other_vertCount = connectEndTo.getVertexCount();
                Vector3 other_vert0 = connectEndTo.getVertexWorldPos(other_vertCount - 2);
                Vector3 other_vert1 = connectEndTo.getVertexWorldPos(other_vertCount - 1);

                vertices[vertices.Length - 1] = transform.InverseTransformPoint(other_vert0);
                vertices[vertices.Length - 2] = transform.InverseTransformPoint(other_vert1);

                //connect splines
                BezierSpline other = connectEndTo.getSpline();

                Vector3 other_end = other.transform.TransformPoint(other.GetControlPoint(other.ControlPointCount - 1));
                spline.SetControlPoint(spline.ControlPointCount - 1, transform.InverseTransformPoint(other_end));
            }
        }


        //assign new vertices/uvs/triangles
        lineMesh.vertices = vertices;
        lineMesh.uv = uvs;
        lineMesh.triangles = triangles;

        lineMesh.RecalculateNormals();//important for proper lighting
    }


    public Vector3 getVertexWorldPos(int index)
    {
        return transform.TransformPoint(meshFilter.sharedMesh.vertices[index]);
    }

    public int getVertexCount()
    {
        return meshFilter.sharedMesh.vertexCount;
    }

    public BezierSpline getSpline()
    {
        return spline;
    }
}
