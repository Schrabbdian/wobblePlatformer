﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Collections;

public class MassSpringSystem : MonoBehaviour 
{    
    //parameters
    [Header("Physical Properties")]
    public float frequency = 1;
    public float dampingRatio;
    public float mass = 10;

    [Header("Mass Points")]
    public Transform[] MassPoints;

    [Header("Anchor Points")]
    public Transform[] AnchorPoints;
    public float anchorFrequency = 2.0f;

    [Header("Other Options")]
    public bool addConstantForce2D = false;
    public float polygonColliderSizeOverhead = 0.1f;
     
    //spring-related
    private EdgeCollider2D[] edgeColliders;
    private CircleCollider2D[] circleColliders;
    private List<SpringJoint2D> springs = new List<SpringJoint2D>();
    private List<Rigidbody2D> pointRigidBodies = new List<Rigidbody2D>();


    //scaling
    private List<Vector2> original_springDistances = new List<Vector2>();
    private List<float> original_circleColSizes = new List<float>();
    private Vector3 original_scale;
    private Vector3 current_scale;

    //per point components
    private List<ConstantForce2D> constantForces = new List<ConstantForce2D>();


    //internal variables for physical behaviour
    private Vector2 currentConstForce = Vector2.zero;
    private float currentDeformation = 0.0f;

    private PolygonCollider2D polygonCollider;


	// Initialization
	void Awake ()
    {
        polygonCollider = GetComponent<PolygonCollider2D>();
        edgeColliders = new EdgeCollider2D[MassPoints.Length];
        circleColliders = new CircleCollider2D[MassPoints.Length];

        original_scale = transform.localScale;//save original scale
        current_scale = original_scale;

        #region Set Up Mass Points

        //Set up Mass Points and Springs:
        for (int i = 0; i < MassPoints.Length; i++)
        {
            //add a "MassPoint" script to each point
            var mp = MassPoints[i].gameObject.AddComponent<MassPoint>();

            // set parent reference, as this will be lost otherwise with unparenting
            // this is needed for collision collection
            mp.parent = this.gameObject;

            //store circle collider of every mass point
            circleColliders[i] = MassPoints[i].GetComponent<CircleCollider2D>();
            original_circleColSizes.Add(circleColliders[i].radius);

            //add an edge collider to every mass point
            edgeColliders[i] = MassPoints[i].gameObject.AddComponent<EdgeCollider2D>();
            edgeColliders[i].sharedMaterial = circleColliders[i].sharedMaterial;
            edgeColliders[i].edgeRadius = circleColliders[i].radius;

            if (addConstantForce2D) { constantForces.Add(MassPoints[i].gameObject.AddComponent<ConstantForce2D>()); }//adds constant force if selected

            //TODO dont add duplicate springs between 2 different mass points

            //add spring joints between all pairs of different points
            for (int j = 0; j < MassPoints.Length; j++)
            {
                if (j != i)
                {
                    SpringJoint2D spring = MassPoints[i].gameObject.AddComponent<SpringJoint2D>();

                    spring.connectedBody = MassPoints[j].GetComponent<Rigidbody2D>();//connect with the other rigidbody
                    Vector2 originalDistVector = MassPoints[i].position - MassPoints[j].position;
                    spring.distance = originalDistVector.magnitude;

                    original_springDistances.Add(originalDistVector);//save original distance vector of the spring

					// if enabled (which it is by default) the Systems will just 
					// collapse on OSX for some strange reason.
					// Only god knows why
					spring.autoConfigureDistance = false;

                    springs.Add(spring);
                }
            }
        }

        //Store references to all rigidbodies of masspoints
        for (int i = 0; i < MassPoints.Length; i++)
        {
            pointRigidBodies.Add(MassPoints[i].GetComponent<Rigidbody2D>());
        }

        
        //disable collisions with MassPoints from the same system
        for (int i = 0; i < MassPoints.Length; i++)
        {
            Physics2D.IgnoreCollision(edgeColliders[i], circleColliders[i]);

            if(polygonCollider) 
            {
                Physics2D.IgnoreCollision(polygonCollider, circleColliders[i]);
                Physics2D.IgnoreCollision(polygonCollider, edgeColliders[i]);
            }

            for (int j = MassPoints.Length - 1; j > i; j--)
            {
                Physics2D.IgnoreCollision(edgeColliders[i], edgeColliders[j]);
                Physics2D.IgnoreCollision(edgeColliders[i], circleColliders[j]);
                Physics2D.IgnoreCollision(edgeColliders[j], circleColliders[i]);
                Physics2D.IgnoreCollision(circleColliders[i], circleColliders[j]);
            }
        }
        #endregion

        #region Set Up Anchor Points
        foreach (Transform anchorPoint in AnchorPoints)
        {
            //unparent and hide anchor point
            anchorPoint.parent = null;
            anchorPoint.hideFlags = HideFlags.HideInHierarchy; //hide anchorpoint by default

            //add a spring joint that connects the anchor to every mass point
            for (int j = 0; j < MassPoints.Length; j++)
            {
                SpringJoint2D spring = anchorPoint.gameObject.AddComponent<SpringJoint2D>();

                spring.connectedBody = MassPoints[j].GetComponent<Rigidbody2D>();//connect with the Mass Point

                Vector2 originalDistVector = anchorPoint.position - MassPoints[j].position;
                spring.distance = originalDistVector.magnitude;
                spring.frequency = anchorFrequency;


                // if enabled (which it is by default) the Systems will just 
                // collapse on OSX for some strange reason.
                // Only god knows why
                spring.autoConfigureDistance = false;
            }
        }

        #endregion

        //Initialize parameters
        updateSpringParameters();
        updateRigidBodyParameters();

    }
        
    //Physics Update
    void Update()
    {
        adjustCollider();

        currentDeformation = -1.0f;//set current deformation as "dirty" (not yet calculated)
    }

    public float CalculateArea()
    {
        float area = 0;
        int n = MassPoints.Length;

        if (n < 3) return 0.0f;  // a degenerate polygon (no area)

        for (int i = 1; i < n; i++)
        {
            int j = (i + 1) % n;//next vertex
            int k = (i - 1) % n;// previous vertex
            area += MassPoints[i].position.x * (MassPoints[j].position.y - MassPoints[k].position.y);//area of formed triangle
        }
        area += MassPoints[0].position.x * (MassPoints[1].position.y - MassPoints[n - 1].position.y);  // wrap-around term
        return Mathf.Abs(area / 2.0f);
    }

    /*
    private void calculateDeformation()
    {
        float deformationStrength = 0.0f;

        foreach (SpringJoint2D spring in springs)
        {
            //get the actual distance between the connected mass points
            float actualDistance = Vector2.Distance(spring.transform.position, spring.connectedBody.position);
            float targetDistance = spring.distance;

            //calculate & sum deformation
            float deformation = Mathf.Abs(actualDistance - targetDistance);

            deformationStrength += deformation;
        }

        deformationStrength /= springs.Count;
        deformationStrength = (float)System.Math.Round(deformationStrength, 4);//round so the number becomes legible
        currentDeformation = deformationStrength;
    }*/

    private void calculateDeformation()
    {
        float deformationMax = 0.0f;

        foreach (SpringJoint2D spring in springs)
        {
            //get the actual distance between the connected mass points
            float actualDistance = Vector2.Distance(spring.transform.position, spring.connectedBody.position);
            float targetDistance = spring.distance;

            //calculate & sum deformation
            float deformation = Mathf.Abs(1.0f - (actualDistance/targetDistance));

            if (deformation > deformationMax) { deformationMax = deformation; }
        }

        deformationMax = (float)System.Math.Round(deformationMax, 4);//round so the number becomes legible
        currentDeformation = deformationMax;
    }

    private void adjustCollider()
    {
        if (MassPoints.Length <= 0) return;

        // create this only once, as setting the points to the collider
        // does copy magic it seems
        Vector2[] points = new Vector2[3];


        //for all edge colliders
        for(int i = 0; i < edgeColliders.Length; i++)
        {
            Vector2 ownPos = MassPoints[i].InverseTransformPoint(MassPoints[i].position);
            Vector2 previousPos = MassPoints[i].InverseTransformPoint(MassPoints[((i - 1) + MassPoints.Length) % MassPoints.Length].position);
            Vector2 nextPos = MassPoints[i].InverseTransformPoint(MassPoints[((i + 1) + MassPoints.Length) % MassPoints.Length].position);

            //edgePoints are in local space --> transform
            points[0] = previousPos;
            points[1] = ownPos;
            points[2] = nextPos;

            edgeColliders[i].points = points;

            // adjust offset of circle colliders
            // Vector2 line = edgePoints[0] - edgePoints[2]; //the line connecting the ends of the edge collider
            // Vector2 normal = new Vector2(-line.y, line.x).normalized;

            /*circleColliders[i].offset = new Vector2(normal.x * (circleColliders[i].radius + 0.1f * transform.localScale.x / original_scale.x),
                                                    normal.y * (circleColliders[i].radius + 0.1f * transform.localScale.y / original_scale.y));*/
        }

        /*
        //for all circle colliders
        for (int i = 0; i < circleColliders.Length; i++)
        {
            //adjust collider size based on current scale
            circleColliders[i].radius = original_circleColSizes[i] * Mathf.Min(current_scale.x / original_scale.x, current_scale.y / original_scale.y);
        }*/


        //adjust the polygon collider if there is one attached (polygon collider should be trigger)
        if (polygonCollider != null)
        {
            Vector2[] pointPositions = new Vector2[MassPoints.Length];

            for (int i = 0; i < MassPoints.Length; i++)
            {
                Vector2 pos = MassPoints[i].position;
                pointPositions[i] = transform.InverseTransformPoint(pos) * (1.0f + polygonColliderSizeOverhead);
            }

            polygonCollider.points = pointPositions;
        }
        



    }

    private void moveToMidpoint()
    {
        //calculate average position of mass points
        Vector2 avgPos = Vector2.zero;

        foreach(Rigidbody2D r in pointRigidBodies)
        {
            avgPos += r.position;
        }

        avgPos /= pointRigidBodies.Count;


        //Move to average position
        transform.position = new Vector3(avgPos.x, avgPos.y, transform.position.z);

    }


    private void updateSpringParameters()
    {
        for (int i = 0; i < springs.Count; i++)
        {
            springs[i].frequency = frequency;
            springs[i].dampingRatio = dampingRatio;
 
        }
    }

    private void updateRigidBodyParameters()
    {
        for (int i = 0; i < pointRigidBodies.Count; i++)
        {
            pointRigidBodies[i].mass = mass / MassPoints.Length;
        }
    }


    public void AddForce(Vector2 force)
    {
        Vector2 forcePerPoint = force.normalized * (force.magnitude / pointRigidBodies.Count);

        for (int i = 0; i < pointRigidBodies.Count; i++)
        {
            pointRigidBodies[i].AddForce(forcePerPoint);
        }

    }

    public void AddForce(Vector2 force, ForceMode2D mode)
    {
        Vector2 forcePerPoint = force.normalized * (force.magnitude / pointRigidBodies.Count);

        for (int i = 0; i < pointRigidBodies.Count; i++)
        {
            pointRigidBodies[i].AddForce(forcePerPoint, mode);
        }

    }

    public void MovePosition(Vector2 position)
    {
        Vector2 displacement = (position - (Vector2)transform.position);

        for (int i = 0; i < pointRigidBodies.Count; i++)
        {
            pointRigidBodies[i].MovePosition(pointRigidBodies[i].position + displacement);
        }
    }

    public void SetPosition(Vector2 position)
    {
        Vector2 displacement = (position - (Vector2)transform.position);

        for (int i = 0; i < pointRigidBodies.Count; i++)
        {
            pointRigidBodies[i].transform.position = (pointRigidBodies[i].transform.position + (Vector3)displacement);
        }

        moveToMidpoint();
    }

    public Vector2 getVelocity()
    {
        Vector2 result = Vector2.zero;
        for (int i = 0; i < pointRigidBodies.Count; i++)
        {
            result += pointRigidBodies[i].velocity;
        }

        result /= pointRigidBodies.Count;

        return result;
    }

    public void setVelocity(Vector2 vel)
    {
        for (int i = 0; i < pointRigidBodies.Count; i++)
        {
            pointRigidBodies[i].velocity = vel;
        }
    }

    public List<Rigidbody2D> getPointRigidbodies()
    {
        return pointRigidBodies;
    }

    public CircleCollider2D[] getPointColliders()
    {
        return circleColliders;
    }

    public void setFrequency(float newFrequency)
    {
        frequency = newFrequency;

        this.updateSpringParameters();
    }

    public void setConstantForce(Vector2 force)
    {
        if(!addConstantForce2D) return;

        currentConstForce = force;

        foreach(ConstantForce2D cForce in constantForces)
        {
            cForce.force = force;
        }
    }

    public Vector2 getConstantForce()
    {
        return currentConstForce;
    }

    public float getDeformation()
    {
        //only calculate the deformation if requested and "dirty" (not yet calculated)
        if (currentDeformation < 0.0f)
        {
            calculateDeformation();
        }

        return currentDeformation;//return newly calculated / stored value
    }

    //get scale masspoints
    public Vector3 GetScale()
    {
        return current_scale;
    }

    //set scale
    public void SetScale(Vector2 scale)
    {
        if (scale.x == 0 || scale.y == 0)
            return;

        current_scale = scale;

        for (int i = 0; i < springs.Count; i++)
        {
            SpringJoint2D springjoint = springs[i];
            Vector2 originalSpringDistance = original_springDistances[i];

            Vector2 newDistance;
            newDistance.x = originalSpringDistance.x * (current_scale.x / original_scale.x);
            newDistance.y = originalSpringDistance.y * (current_scale.y / original_scale.y);

            springjoint.distance = newDistance.magnitude;
        }
    }

    public void SetMass(float m)
    {
        mass = m;

        for (int i = 0; i < pointRigidBodies.Count; i++)
        {
            pointRigidBodies[i].mass = mass / MassPoints.Length;
        }
    }

    public void SetIgnoreCollision(Collider2D collider, bool active)
    {
        foreach (Collider2D c in circleColliders)
        {
            Physics2D.IgnoreCollision(collider, c, active);
        }

        foreach (Collider2D c in edgeColliders)
        {
            Physics2D.IgnoreCollision(collider, c, active);
        }
    }

    public void SetIgnoreCollision(MassSpringSystem ms, bool active)
    {
        foreach (Collider2D c in circleColliders)
        {
            ms.SetIgnoreCollision(c, active);
        }

        foreach (Collider2D c in edgeColliders)
        {
            ms.SetIgnoreCollision(c, active);
        }
    }

    public void SetCollidersEnabled(bool enabled)
    {
        foreach (Collider2D c in circleColliders)
        {
            c.enabled = enabled;
        }

        foreach (Collider2D e in edgeColliders)
        {
            e.enabled = enabled;
        }
    }

    public void SetActive(bool active)
    {
        //note: this method has to be as ugly as it is since the activations/deactivations have to happen in reversed order
        if (!active)
        {
            //freeze system first
            foreach (Rigidbody2D r in pointRigidBodies)
            {
                r.simulated = false;
            }

            //disable spring joints
            foreach (SpringJoint2D s in springs)
            {
                s.enabled = false;
            }


            //disable all points
            foreach (Transform t in MassPoints)
            {
                t.gameObject.SetActive(false);
            }

            foreach (Transform t in AnchorPoints)
            {
                t.gameObject.SetActive(false);
            }

            gameObject.SetActive(false);//and the main object
        }
        else
        {
            gameObject.SetActive(true);//enable the main object

            //enable all points
            foreach (Transform t in MassPoints)
            {
                t.gameObject.SetActive(true);
            }

            foreach (Transform t in AnchorPoints)
            {
                t.gameObject.SetActive(true);
            }

            //enable spring joints
            foreach (SpringJoint2D s in springs)
            {
                s.enabled = true;
            }

            //unfreeze
            foreach (Rigidbody2D r in pointRigidBodies)
            {
                r.simulated = true;
            }
        }
    }

    //only in editor
    public List<SpringJoint2D> GetSprings()
    {
        return springs;
    }
}