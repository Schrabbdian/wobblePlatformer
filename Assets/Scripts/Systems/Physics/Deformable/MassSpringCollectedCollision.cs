﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Implement this interface if you want to be notified by the collisions 
///  coming from the mass spring system
///  Note that it works a bit differently from just feeding up all the 
///  collision information
/// </summary>
public interface MassSpringCollectedCollision 
{
    /// <summary>
    ///  If the attached MassSpringSystem registers a NEW collision Enter
    ///  with a GameObject, that was not colliding in the previous physics 
    ///  step this will call this method with the corresponding collision object.
    /// 
    ///  Note that if other CollisionEnter events occur from other mass points
    ///  this will NOT be called unless the other GameObject has stopped colliding
    ///  in the physics step before this.
    /// 
    ///  If you want to access collision information every frame please use
    ///  GetCollision2DSetFor(GameObject) in FixedUpdate()
    /// </summary>
    /// <param name="coll">Collision object of the collision causing the enter event</param>
    /// <param name="resolvedObject">
    ///  If colliding with mass spring system this holds the mass spring system
    ///  If colliding with normal other object this is the object itself
    /// </param>
    void OnCollisionEnterColl(Collision2D coll,GameObject resolvedObject);

    /// <summary>
    ///  Similarily to OnCollisionEnterColl this is called when a GameObject
    ///  has completely stopped colliding with the attached MassSpringSystem
    /// </summary>
    /// <param name="coll">Collision object of the collision causing the exit event</param>
    /// <param name="resolvedObject">
    ///  If colliding with mass spring system this holds the mass spring system
    ///  If colliding with normal other object this is the object itself
    /// </param>
    void OnCollisionExitColl(Collision2D coll, GameObject resolvedObject);

    /// <summary>
    ///  Also see OnCollisionEnterColl.
    ///  This is called once per FixedUpdate while a collision between two objects 
    ///  is still active.
    /// 
    ///  Also just like OnCollisionEnterColl it will not trigger more than once 
    ///  per FixedUpdate when colliding with a MassSpringSystem.
    ///  So you will only get the FIRST Collision2D from the Collision Stay with
    ///  a MassSpringSystem, as there might be more than one Collision2D currently active
    /// </summary>
    /// <param name="coll">Collision object of the collision causing the stay event</param>
    /// <param name="resolvedObject">
    ///  If colliding with mass spring system this holds the mass spring system
    ///  If colliding with normal other object this is the object itself
    /// </param>
    void OnCollisionStayColl(Collision2D col, GameObject resolvedObject);
}
