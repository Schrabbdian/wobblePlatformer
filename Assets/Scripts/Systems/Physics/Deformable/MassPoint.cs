﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


//Forwards Collision Events to scripts in parent
[RequireComponent(typeof(Rigidbody2D))]
public class MassPoint : MonoBehaviour
{
    public GameObject parent;
    private MassSpringCollisionCollector collisionCollector;
    private MassSpringSystem massSpring;

    void Awake()
    {
        //store reference to mass Spring system
        massSpring = GetComponentInParent<MassSpringSystem>();

        collisionCollector = GetComponentInParent<MassSpringCollisionCollector>();

        //hide this masspoint on default
        gameObject.hideFlags = HideFlags.HideInHierarchy;
    }


    public MassSpringSystem GetSystem()
    {
        return massSpring;
    }

    // Forward all events to the collector
    // (if there is a collector on this game object)

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (collisionCollector != null)
            collisionCollector.RegisterOnPointCollisionEnter2D(coll);
    }

    void OnCollisionStay2D(Collision2D coll)
    {
        if (collisionCollector != null)
            collisionCollector.RegisterOnPointCollisionStay2D(coll);
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        if (collisionCollector != null)
            collisionCollector.RegisterOnPointCollisionExit2D(coll);
    }
}