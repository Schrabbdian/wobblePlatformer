﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class DeformMesh : MonoBehaviour 
{
    //public parameters
    public Transform[] cagePoints = new Transform[4];//points of the cage that controls deformation

    //components
    private Mesh mesh;
    private Material mat;

    //internal variables
    private Vector3 original_cagePoint0;//used to calculate deform rotation
    
    //Variables for passing stuff to GPU
    private ComputeBuffer cagePos_buffer;
    private ComputeBuffer weights_buffer;
    private ComputeBuffer staticPos_buffer;
    
    private Vector3[] cagePos;
    private float[] weights;
    private Vector3[] staticPos;

    // cagePos for Fallbackmode, must be Vector4
    private Vector4[] cagePosFallBack;

	// Use this for initialization
	void Start () 
    {
        //get component references
        SkinnedMeshRenderer skinnedRenderer = GetComponent<SkinnedMeshRenderer>();
        if (skinnedRenderer != null)
        {
            mat = skinnedRenderer.material;
            mesh = skinnedRenderer.sharedMesh;
        }
        else
        {
            MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
            MeshFilter meshFilter = GetComponent<MeshFilter>();
            if (meshRenderer != null && meshFilter != null)
            {
                mat = meshRenderer.material;
                mesh = meshFilter.mesh;
            }
            else
            {
                Debug.LogError("Mesh Components not set up correctly!");
            }
        }

        //create CPU arrays
        cagePos = new Vector3[cagePoints.Length];
        weights = new float[cagePoints.Length * mesh.vertices.Length];
        staticPos = new Vector3[mesh.vertices.Length];
     
        // compute all needed values

        //calculate original position of cage points in local space
        for (int i = 0; i < cagePoints.Length; i++)
        {
            cagePos[i] = transform.InverseTransformPoint(cagePoints[i].position);
        }
            
        //save original position of first cage point
        if (cagePos.Length >= 1) { original_cagePoint0 = cagePos[0]; }

        //save original (undeformed) vertex positions
        for (int i = 0; i < mesh.vertexCount; i++)
        {
            staticPos[i] = mesh.vertices[i];
        }

        //precompute the mean value weights that will be used to deform the mesh
        calculateMeanValueWeights();


        if( SystemInfo.graphicsShaderLevel >= 50 )
        {
            //create buffers
            cagePos_buffer = new ComputeBuffer(cagePos.Length, sizeof(float) * 3);
            weights_buffer = new ComputeBuffer(weights.Length, sizeof(float));
            staticPos_buffer = new ComputeBuffer(staticPos.Length, sizeof(float) * 3);

            //bind buffers
            mat.SetBuffer("cagePoints", cagePos_buffer);//bind cagePos_buffer to GPU variable "cagePoints"
            mat.SetBuffer("weights", weights_buffer);//bind weights_buffer to GPU variable "weights"
            mat.SetBuffer("staticPosition", staticPos_buffer);//bind staticPos_buffer to GPU variable "staticPosition"

            cagePos_buffer.SetData(cagePos);//actually set data
            mat.SetInt("cagePoints_count",cagePos_buffer.count);

            staticPos_buffer.SetData(staticPos);//actually set data

            weights_buffer.SetData(weights);//actually set data
        }
        else
        // fallback mode
        // this is only used if StructuredBuffers are not avaliable
        {
            #if UNITY_EDITOR
            Debug.Log("<color=teal>using fallback mode for deformation shader</color>");
            #endif


            // this number is somewhat arbitrary, but if this limit is changed, it must
            // be changed in the shader too
            if (cagePos.Length > 512)
            {
                Debug.LogError("For Fallbackmode, cagePoints must not exceed 512 points!");
            }

            // create the fall back cage pos array
            cagePosFallBack = new Vector4[cagePos.Length]; 

            // otherwise pass the data to the shader via texture
            // for this create a new texture with dimensions cagePos x vertices

            // this is how many weights exist
            int _weightsCount = cagePos.Length* mesh.vertices.Length;
           
            // every platform has a different maximum amount of texture size
            int MAX_TEXTURE_SIZE = SystemInfo.maxTextureSize;


            // this calculates how many pixels will be needed for the weight
            // one pixel can store 4 weights (RGBA)
            int _neededPixels = _weightsCount / 4 + (_weightsCount % 4 != 0 ? 1 : 0);

            // how big must one side of the texture be?
            int _weights_power = 1 + (int) (Mathf.Log(Mathf.Sqrt(_neededPixels)) / Mathf.Log(2f));
            int _weights_texture_size = (int) Mathf.Pow(2f, (float)_weights_power);

            // abort if too large
            if (_weights_texture_size > MAX_TEXTURE_SIZE)
            {
                Debug.LogError("Needed texture size of " + _weights_texture_size + " but only " + MAX_TEXTURE_SIZE + " is avaliable!");
                return;
            }

            Texture2D weightsTexture = new Texture2D(_weights_texture_size,_weights_texture_size);

            // for every vertex x cagePos combination the weigth must be stored
            // as one weight only takes one color channel, 4 weights are stored
            // in one pixel
            for (int i = 0; i < mesh.vertices.Length; i++)
            {
                for (int j = 0; j < cagePos.Length; j++)
                {
                    int _index = i * cagePos.Length + j;
                    int _rgbaIndex = _index % 4;
                    int _rowIndex = (_index / 4) / _weights_texture_size;
                    int _colIndex = (_index / 4) % _weights_texture_size;
                        
                    Color color = weightsTexture.GetPixel(_rowIndex, _colIndex);
                    if (_rgbaIndex == 0)
                        color.r = weights[_index];
                    else if (_rgbaIndex == 1)
                        color.g = weights[_index];
                    else if (_rgbaIndex == 2)
                        color.b = weights[_index];
                    else if (_rgbaIndex == 3)
                        color.a = weights[_index];
                    
                    weightsTexture.SetPixel(_rowIndex, _colIndex, color);
                }
            }
            weightsTexture.Apply();


            // for every static pos three pixels are needed
            int _static_pos_count = staticPos.Length*3;
            int _static_pos_power = 1 + (int) (Mathf.Log(Mathf.Sqrt(_static_pos_count)) / Mathf.Log(2f));
            int _static_pos_texture_size = (int) Mathf.Pow(2f, (float)_static_pos_power);
            // no checking required, this is smaller then before
            Texture2D staticPosTexture = new Texture2D(_static_pos_texture_size, _static_pos_texture_size);

            const float MAX_VALUE_RANGE = 100f;
            for (int i = 0; i < staticPos.Length; i++)
            {
                // as this only saves one position (for example pos.x) 
                // three entries in the texture are needed to store the full x,y,z
                // this three sucessive pixels are used to store the static position

                float x = Mathf.Clamp(staticPos[i].x, -MAX_VALUE_RANGE, MAX_VALUE_RANGE);
                float y = Mathf.Clamp(staticPos[i].y, -MAX_VALUE_RANGE, MAX_VALUE_RANGE);
                float z = Mathf.Clamp(staticPos[i].z, -MAX_VALUE_RANGE, MAX_VALUE_RANGE);

                int _index = i * 3;

                SaveStaticPosToIndex(x,_index, staticPosTexture, MAX_VALUE_RANGE);
                SaveStaticPosToIndex(y,_index+1,staticPosTexture, MAX_VALUE_RANGE);
                SaveStaticPosToIndex(z,_index+2, staticPosTexture, MAX_VALUE_RANGE);
            }
            staticPosTexture.Apply();
                

            mat.SetTexture("_VertexWeights", weightsTexture);
            mat.SetInt("cagePointsCount", cagePos.Length);
            mat.SetInt("vertexCount", mesh.vertices.Length);
            mat.SetInt("_weights_texture_size", _weights_texture_size);
            mat.SetInt("_static_pos_texture_size", _static_pos_texture_size);
            mat.SetTexture("_StaticPositions", staticPosTexture);
            mat.SetFloat("VAL_RANGE", MAX_VALUE_RANGE);
        }

        //unparent all cage points
        foreach (Transform t in cagePoints)
        {
            t.parent = null;
            t.gameObject.hideFlags = HideFlags.HideInHierarchy; //hide cagepoint by default
        }
	}

    // helper function for fallback mode
    // given a position, and index to store at and a texture
    // this saves the given position into the pixel
    private void SaveStaticPosToIndex(float pos, int index, Texture2D tex, float val_Range)
    {
        // this calculates the static position
        // as color values are only in [0,1] range,
        // this compresses the values to fit into
        // 3x [0,1] 
        // (just storing in one value resulted in very strange results???)
        // for this in the first value saves whether the position was positive or negative
        // 0 for negative; 1 for positive
        // the second value stores the number after the comma of the position
        // the second value stores the number before the comma divided by max_range
        // not that the larger the range, the less precise the number gets!

        int _IndexRow = index / tex.width;
        int _IndexCol = index % tex.width;
        float val = pos;
        Color color = new Color();
        color.r = val < 0f ? 0f : 1f;
        color.g = val < 0f ? -val - ((int)(-val)) : val - ((int)val);
        color.b = val < 0f ? (-val) - color.g : val - color.g;
        color.b /= val_Range;
        tex.SetPixel(_IndexRow,_IndexCol,color);
    }

    void LateUpdate()
    {
        //update positions of cage points (relative to origin)
        for (int i = 0; i < cagePoints.Length; i++)
        {
            cagePos[i] = transform.InverseTransformPoint(cagePoints[i].position);
        }

        //create data
        if (SystemInfo.graphicsShaderLevel >= 50)
        {
            cagePos_buffer.SetData(cagePos);//actually set data
        }
        else
        {
            for (int i = 0; i < cagePoints.Length; i++)
            {
                cagePosFallBack[i].x = cagePos[i].x;
                cagePosFallBack[i].y = cagePos[i].y;
                cagePosFallBack[i].z = cagePos[i].z;
            }
            mat.SetVectorArray("cagePointsArr", cagePosFallBack);
        }


        //calculate the rotation of the cage and pass it to shader
        if (cagePos.Length >= 1)
        {
            Vector2 up = original_cagePoint0;
            Vector2 dir = cagePos[0];

            float det = up.x * dir.y - dir.x * up.y;
            float dot = up.x * dir.x + up.y * dir.y;

            float angle = -Mathf.Atan2(det, dot);


            mat.SetFloat("_deformRotation", angle);
        }

    }

    void FixedUpdate()
    {
        moveToMidpoint();
    }

    void OnDestroy()
    {
        if (cagePos_buffer != null && weights_buffer != null && staticPos_buffer != null)
        {
            //release GPU buffers
            cagePos_buffer.Release();
            weights_buffer.Release();
            staticPos_buffer.Release();
        }
    }

    private void calculateMeanValueWeights()//calculate the weights of each cage point for every vertex
    {
        Vector3[] vertices = mesh.vertices;
        float[] angles = new float[weights.Length];

        for(int i = 0; i < vertices.Length; i++)
        {
            Vector3 vertex_pos = vertices[i];

            //Step 1: calculate angles alpha for every triangle from current Vertex to all cage points
            for (int j = 0; j < cagePos.Length; j++)
            {
                Vector2 dir1 = new Vector2(cagePos[j].x - vertex_pos.x, cagePos[j].y - vertex_pos.y).normalized;
                Vector2 dir2 = new Vector2(cagePos[(j + 1) % cagePos.Length].x - vertex_pos.x, cagePos[(j + 1) % cagePos.Length].y - vertex_pos.y).normalized;

                float angle = Vector2.Angle(dir2, dir1);
                angles[i * cagePos.Length + j] = angle;
            }

            //Step 2: calculate weights
            for (int j = 0; j < cagePos.Length; j++)
            {
                float angle_j = angles[i * cagePos.Length + j];
                float angle_j_minus1 = angles[i * cagePos.Length + ((j - 1 + cagePos.Length) % cagePos.Length)];

                float distance = new Vector2(cagePos[j].x - vertex_pos.x, cagePos[j].y - vertex_pos.y).magnitude;

                float weight_j = (Mathf.Tan(Mathf.Deg2Rad * (angle_j / 2.0f)) + Mathf.Tan(Mathf.Deg2Rad * (angle_j_minus1 / 2.0f))) / distance;
                weights[i * cagePos.Length + j] = weight_j;
            }
            
            //Step 3: normalize weights
            float weight_sum = 0;
            for (int j = 0; j < cagePos.Length; j++)//sum up all weights for this vertex
            {
                weight_sum += weights[i * cagePos.Length + j];
            }

            for (int j = 0; j < cagePos.Length; j++)//divide every weight by sum
            {
                weights[i * cagePos.Length + j] /= weight_sum;
            }
        }
    }

    private void moveToMidpoint()
    {
        //calculate average position of mass points
        Vector2 avgPos = Vector2.zero;

        foreach (Transform p in cagePoints)
        {
            avgPos += (Vector2)p.position;
        }

        avgPos /= cagePoints.Length;


        //Move to average position
        transform.position = new Vector3(avgPos.x, avgPos.y, transform.position.z);

    }
}
