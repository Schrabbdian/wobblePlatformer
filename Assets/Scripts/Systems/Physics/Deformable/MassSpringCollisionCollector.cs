﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Add this component to a GameObject with an attached MassSpring System
///  and this will be able to collect the collisions per physics step and forward
///  them only once not too often
/// </summary>
public class MassSpringCollisionCollector : MonoBehaviour 
{
    private class CollisionSet<T>
    {
        private Dictionary<GameObject,Dictionary<GameObject,T>> collisionInformation = new Dictionary<GameObject,Dictionary<GameObject,T>>();

        public void SaveCollision(GameObject thisMassPoint, GameObject otherObject,T collInformation)
        {
            // if this mass point is not even registered create the entry
            if (!collisionInformation.ContainsKey(thisMassPoint))
            {
                collisionInformation.Add(thisMassPoint, new Dictionary<GameObject, T>());
                collisionInformation[thisMassPoint].Add(otherObject, collInformation);
            }
            // if only the other game object is not registered do that
            else if (!collisionInformation[thisMassPoint].ContainsKey(otherObject))
                collisionInformation[thisMassPoint].Add(otherObject, collInformation);
            // if both are registered only update the collision information
            else
                collisionInformation[thisMassPoint][otherObject] = collInformation;
        }

        public void ForgetCollision(GameObject thisMassPoint, GameObject otherObject)
        {
            // if the object correlation was saved remove it
            if (collisionInformation.ContainsKey(thisMassPoint)
                && collisionInformation[thisMassPoint].ContainsKey(otherObject))
            {
                collisionInformation[thisMassPoint].Remove(otherObject);

                // also if the dicts are empty remove those
                // to not eat all memory
                if (collisionInformation[thisMassPoint].Count == 0)
                    collisionInformation.Remove(thisMassPoint);
            }
        }

        public bool IsEmpty()
        {
            return collisionInformation.Count == 0;
        }

        public void Print()
        {
            foreach (var key1 in collisionInformation.Keys)
            {
                Debug.Log(key1.GetHashCode());
                foreach (var key2 in collisionInformation[key1].Keys)
                {
                    Debug.Log("\t" + key2.GetHashCode());
                }
            }
        }
    }

    struct CollisionTuple
    {
        public GameObject a;
        public GameObject b;
    }

    private MassSpringCollectedCollision[] listener;

    // contains all collected collisions from the last physics update
    // this relates the collided game objects with all the colliding mass points and the respective collision information
    private Dictionary<GameObject,CollisionSet<Collision2D>> pointCollisionSet = new Dictionary<GameObject,CollisionSet<Collision2D>>();
    private Dictionary<GameObject,HashSet<GameObject>> pointCollisionStaySet = new Dictionary<GameObject,HashSet<GameObject>>();
    private Dictionary<GameObject,GameObject> massSpringCacheMapCollision = new Dictionary<GameObject, GameObject>();


    public bool CollectCollisionEnter = true;
    public bool CollectCollisionStay = false;
    public bool CollectCollisionExit = false;

    public void Awake()
    {
        listener = GetComponents<MassSpringCollectedCollision>();
    }

    public void RegisterOnPointCollisionEnter2D(Collision2D coll)
    {
        #if UNITY_EDITOR
        UnityEngine.Profiling.Profiler.BeginSample("CollisionEnter2DCollect");
        #endif

        var otherObject = coll.gameObject;
        var otherObjectReal = otherObject;
        var thisMassPoint = coll.otherCollider.gameObject;

        // if other object is mass spring system 
        // do not collide with MassPoint but with GameObject
        // When CollisionEnter try to get MassPoint Script
        // as Enter is sometimes called multiple times
        // try to find if it is in the cache map at first
        if (massSpringCacheMapCollision.ContainsKey(otherObject))
            otherObjectReal = massSpringCacheMapCollision[otherObject];
        else
        {
            MassPoint mp = otherObject.GetComponent<MassPoint>();
            if (mp != null)
            {
                massSpringCacheMapCollision.Add(mp.gameObject, mp.parent);
                otherObjectReal = mp.parent;
            }
        }
            
        if (!pointCollisionSet.ContainsKey(otherObjectReal))
        {
            var collisionSet = new CollisionSet<Collision2D>();
            collisionSet.SaveCollision(thisMassPoint,otherObject,coll);
            pointCollisionSet.Add(otherObjectReal, collisionSet);
            foreach( var l in listener )
                l.OnCollisionEnterColl(coll,otherObjectReal);
        }
        else
            pointCollisionSet[otherObjectReal].SaveCollision(thisMassPoint,otherObject,coll);

        #if UNITY_EDITOR
        UnityEngine.Profiling.Profiler.EndSample();
        #endif
    }

    public void RegisterOnPointCollisionStay2D(Collision2D coll)
    {
        if (!CollectCollisionStay)
            return;

        #if UNITY_EDITOR
        UnityEngine.Profiling.Profiler.BeginSample("CollisionStay2DCollect");
        #endif

        var otherObject = coll.gameObject;
        var otherObjectReal = otherObject;
        var thisMassPoint = coll.otherCollider.gameObject;

        // if other object is mass spring system 
        // do not collide with MassPoint but with GameObject
        // if stay, one can use CacheMap
        if (massSpringCacheMapCollision.ContainsKey(otherObject))
            otherObjectReal = massSpringCacheMapCollision[otherObject];

        if (!pointCollisionSet.ContainsKey(otherObjectReal))
        {
            // "This should never happen"
            // but still it does - of course
            // why?
            // Exit is sometimes called before stay
            // also Enter is often called more than once
            // also Exit is often called more than once
            // why?
            // oh only if I'd knew why
            // what now?
            //
            // blissfully ignore this 
            // 
            // if you don't want to ignore this
            // congratulations! Your are the lucky (hehe)
            // winner to get the chance to solve this problem! :P
        }
        else
        {
            // if not yet seen this frame
            // add to internal list
            if (!pointCollisionStaySet.ContainsKey(otherObjectReal))
            {
                var hashset = new HashSet<GameObject>();
                hashset.Add(thisMassPoint);
                pointCollisionStaySet.Add(otherObjectReal, hashset);
            }
            else if (!pointCollisionStaySet[otherObjectReal].Contains(thisMassPoint))
            {
                pointCollisionStaySet[otherObjectReal].Add(thisMassPoint);
            }
            // otherwise if already seen do nothing this frame
            else
            {
                UnityEngine.Profiling.Profiler.EndSample();
                return;
            }

            #if UNITY_EDITOR
            UnityEngine.Profiling.Profiler.BeginSample("CollisionStay2DCollectSaveCall");
            #endif

            pointCollisionSet[otherObjectReal].SaveCollision(thisMassPoint,otherObject,coll);

            foreach( var l in listener )
                l.OnCollisionStayColl(coll,otherObjectReal);

            #if UNITY_EDITOR
            UnityEngine.Profiling.Profiler.EndSample();
            #endif
        }

        #if UNITY_EDITOR
        UnityEngine.Profiling.Profiler.EndSample();
        #endif
    }
        
    public void RegisterOnPointCollisionExit2D(Collision2D coll)
    {
        #if UNITY_EDITOR
        UnityEngine.Profiling.Profiler.BeginSample("CollisionExit2DCollect");
        #endif

        var otherObject = coll.gameObject;
        var otherObjectReal = otherObject;
        var thisMassPoint = coll.otherCollider.gameObject;

        // if other object is mass spring system 
        // do not collide with MassPoint but with GameObject
        // if exit, one can use CacheMap
        if (massSpringCacheMapCollision.ContainsKey(otherObject))
            otherObjectReal = massSpringCacheMapCollision[otherObject];

        if (!pointCollisionSet.ContainsKey(otherObjectReal))
        {
            return;
        }
        
        var collisionSet = pointCollisionSet[otherObjectReal];
        collisionSet.ForgetCollision(thisMassPoint,otherObject);

        if (collisionSet.IsEmpty())
        {
            pointCollisionSet.Remove(otherObjectReal);
            massSpringCacheMapCollision.Remove(otherObject);

            foreach( var l in listener )
                l.OnCollisionExitColl(coll,otherObjectReal);
        }

        #if UNITY_EDITOR
        UnityEngine.Profiling.Profiler.EndSample();
        #endif
    }

    public void FixedUpdate()
    {
        pointCollisionStaySet.Clear();
    }
}
