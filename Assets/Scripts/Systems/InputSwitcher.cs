﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// as every different OS needs to have its own controller mapping
// (see http://wiki.unity3d.com/index.php?title=Xbox360Controller)
// it is necessary to create three instances for every virtual input 
// axis.
// for example
// "Charge"         for Windows
// "Charge_macos"   for macOS
// "Charge_linux"   for Linux
//
// When asking Unity for the input of a given axis please note that
// at first the correct suffix has to be added via GetInputForPlatform(string)
//
// In future releases of Unity this should hopefully be fixed by the New Input System
// but who knows how long this will take

public class InputSwitcher 
{
    // disable the warning that *Modifier variable is never used
    // which is caused as each platform at most uses one modifier
    // (Windows doesn't even use one)
    // so this warning is not necessary
    #pragma warning disable 0414

    // the modifiers that are appended to the base names for all platforms different
    // from windows
    private static string macOSModifier = "_macos";
    private static string linuxModifier = "_linux";

    #pragma warning restore 0414

    // given a base virtual input name, this adds the correct suffix
    // according to the platform the game is compiled to
    public static string GetInputForPlatform(string baseName)
    {
        #if UNITY_STANDALONE_OSX
            return baseName + macOSModifier;
        #elif UNITY_STANDALONE_LINUX
            return baseName + linuxModifier;
        #else
            return baseName;
        #endif
    }
}
