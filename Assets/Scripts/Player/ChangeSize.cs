﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSize : MonoBehaviour 
{
    public Vector2 Size;

    private MassSpringSystem massSpring;
    private bool toggle = false;

	//Initialization
	void Start () 
    {
        //set component references
        massSpring = GetComponent<MassSpringSystem>();
	}
	
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (toggle)
            {
                massSpring.SetScale(Size);
            }
            else
            {
                massSpring.SetScale(Vector2.one);
            }

            toggle = !toggle;
        }
	}
}
