﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

//controls all of the player's movement
[RequireComponent(typeof(MassSpringCollisionCollector))]
public class PlayerMovement : MonoBehaviour, MassSpringCollectedCollision
{
    //input
    [System.Serializable]
    public class InputSettings
    {
        public bool useRawInputAxis = false;

        public string HorizontalInputAxis;
        public string VerticalInputAxis;
        public string chargeButton;

        //[Range(0.0f, 1.0f)]
        //public float axisReleasedCutoff;
    }

    //private float hAxisValueLastFrame = 0;
    //private float vAxisValueLastFrame = 0;
    public InputSettings inputSettings;

    [System.Serializable]
    public class MovementSettings
    {
        //general movement
        public float WalkAcceleration = 30.0f;
        public float FallAcceleration = 30.0f;
        public float AerialAcceleration = 20.0f;
        public float slopeAngle = 60.0f;
        public float squashAngle = 120.0f;
        
        //limits
        [Header("Limits")]
        public float maxSquashForce = 80.0f;
        public Vector2 maxWalkSpeed;
        public Vector2 maxAerialSpeed;
        public float walkingDrag;

        //bouncing
        [Header("Bouncing")]
        public float bounceStrength = 12.0f;
        public float minChargeSpeed = 2.0f;
        public float maxChargeSpeed = 6.0f;
        public float speedForMaxCharge = 5.0f;

        [Header("Super Charge")]
        [Tooltip("Whether the player is able to perform super charge")]
        public bool hasSuperCharge = false;
        [Tooltip("Additive modifier applied to the base bounce strength")]
        public float bounceStrengthModifier = 1.0f;
        [Tooltip("Delay between fully charged jump and onset of super charge state")]
        public float superChargeDelaySec = 1.0f;
        [Tooltip("Seconds how long the jump is a super jump after super charging")]
        public float superJumpDurationSec = 2.0f;
    };

    public MovementSettings moveSettings;



    //component references
    private MassSpringSystem massSpring;
    private List<Rigidbody2D> pointRigidBodies;
    private MeshRenderer mRenderer;

    private Color originalColor;

    private SwallowAbility swallowAbility;

    //collision
    private bool colliding = false;
    //private List<ContactPoint2D> contactPoints = new List<ContactPoint2D>();

    public LayerMask layermask;

    //Bouncing
    private float currentChargeSpeed = 2.0f;
    private bool isCharging = false;
    private float charge = 0.0f;
    private Vector2 wallDir = Vector2.zero;
    private Vector2 bounceDir = Vector2.zero;

    // Super Charge
    private float superChargeDelayCounter = 0.0f;
    private bool superChargeTriggered = false;
    private float superChargeJumpDurationCounter = 0.0f;

    void Start()
    {
        //set component references
        massSpring = gameObject.GetComponent<MassSpringSystem>();
        mRenderer = GetComponent<MeshRenderer>();
        pointRigidBodies = massSpring.getPointRigidbodies();

        originalColor = mRenderer.material.color;

        swallowAbility = GetComponent<SwallowAbility>();
    }

    
    void Update()
    {
        //capture user input
        Vector2 dirInput;

        // Here it is not necessary to get a different axis for each platform as this is actually 
        // the same on different platforms!
        dirInput.x = GetInputAxis(inputSettings.HorizontalInputAxis);
        dirInput.y = GetInputAxis(inputSettings.VerticalInputAxis);

        mRenderer.material.color = superChargeTriggered ?
            Color.yellow : Color.Lerp(originalColor, Color.red, charge);//visualize built up bounce

        mRenderer.material.color = moveSettings.hasSuperCharge && superChargeJumpDurationCounter <= moveSettings.superJumpDurationSec ?
            Color.black : mRenderer.material.color;

        //Disable Movement controls when inhaling or spitting
        if (swallowAbility.GetState() == SwallowAbility.State.Inhaling || swallowAbility.GetState() == SwallowAbility.State.Spitting)
        {
            dirInput = Vector2.zero;
        }


        //calculate average velocity
        Vector2 avgVelocity = massSpring.getVelocity();




        bool aerial = false;

        if (colliding)
        {
            if( Input.GetButton(InputSwitcher.GetInputForPlatform(inputSettings.chargeButton)) ) 
            {
                #region Save Direction when starting charge
                //if starting to charge
                if (charge > 0.05f && !isCharging)
                {
                    Vector2 n = collisionNormal(dirInput);

                    //and input against a surface
                    if (n.sqrMagnitude > 0.0f) 
                    { 
                        isCharging = true;
                        wallDir = dirInput.normalized;
                    }

                }
                #endregion


                Vector2 normal = isCharging ? collisionNormal(wallDir) : collisionNormal(dirInput);

                #region calculate charge build up
                float wallForceFactor = Vector2.Dot(normal, dirInput.normalized);// ~ -1.0: against wall ~0.0: along wall or no input ~1.0: away from walls
                float againstWallFactor = Mathf.Clamp01(0.5f * (-wallForceFactor + 1.0f));

                //calculate charge speed
                float againstWallSpeed = Mathf.Clamp(Vector2.Dot(normal, avgVelocity), 0, moveSettings.speedForMaxCharge);
                currentChargeSpeed = Mathf.Lerp(moveSettings.minChargeSpeed, moveSettings.maxChargeSpeed, againstWallSpeed / moveSettings.speedForMaxCharge);

                float againstWallAngle = Vector2Util.signedAngle(dirInput, normal);

                //only charge if pressing against a wall or already charged (change charge direction more easily)
                if (Mathf.Abs(againstWallAngle) > moveSettings.squashAngle || charge > 0.5f)
                {
                    charge += dirInput.magnitude * currentChargeSpeed * Time.deltaTime * againstWallFactor;
                    charge = Mathf.Clamp01(charge);

                    // If supercharge is available and charge is built up feed the super charge trigger
                    if( moveSettings.hasSuperCharge && !superChargeTriggered && Mathf.Abs(charge-1.0f) <= 0.001f )
                    {
                        superChargeDelayCounter += Time.deltaTime;

                        if( superChargeDelayCounter >= moveSettings.superChargeDelaySec )
                            OnSuperChargeTrigger();
                    }
                }

                //bounce direction
                if (dirInput.sqrMagnitude > 0.05f) { bounceDir = -dirInput.normalized; }

                #endregion

                //apply constant force based on built up bounce
                massSpring.setConstantForce(charge * -normal * moveSettings.maxSquashForce);

                applyDrag();

            }
            else//Basic Ground Movement
            {
                Vector2 groundNormal = collisionNormal(Vector2.down);//TODO: enable arbitrary gravity direction
   
                Vector2 rotatedNormal = new Vector2(groundNormal.y, -groundNormal.x);

                if (groundNormal != Vector2.zero)//if the ray find ground to move on
                {
                    if (Vector2.Angle(groundNormal, Vector2.up) < moveSettings.slopeAngle)//on ground?
                    {
                        Vector2 inputAlongWall = Vector2.Dot(dirInput, rotatedNormal) * rotatedNormal;//input projected onto rotated normal
                        Vector2 accelAlongWall = inputAlongWall * moveSettings.WalkAcceleration * Time.deltaTime;

                        foreach (Rigidbody2D pRB in pointRigidBodies)
                        {
                            applyAcceleration(pRB, accelAlongWall);
                        }

                        applyDrag();
                    }
                }
                else
                {
                    aerial = true;//no ground was found, player should gain aerial control
                }
            }
            
        }

        if(!colliding || aerial) //Aerial Movement
        {
            if(!colliding)
            { 
                charge = Mathf.Clamp01(charge - 10.0f * currentChargeSpeed * Time.deltaTime);//decrease builtup bounce over time
                massSpring.setConstantForce(massSpring.getConstantForce().normalized * charge * moveSettings.maxSquashForce);//reduce force with charge
            }

            //Aerial Movement (Horizontal & down)
            float xAccel = dirInput.x * moveSettings.AerialAcceleration * Time.deltaTime;
            float yAccel = Mathf.Clamp(dirInput.y, -1.0f, 0.0f) * moveSettings.FallAcceleration * Time.deltaTime;


            foreach (Rigidbody2D pRB in pointRigidBodies)
            {
                //only speed up when we are slower than the max movement speed
                Vector2 accel = Vector2.right * xAccel + Vector2.up * yAccel;
                applyAcceleration(pRB, accel);
            }
        }


        //React to button Release
        if (Input.GetButtonUp(InputSwitcher.GetInputForPlatform(inputSettings.chargeButton)))
        {
            OnBounce();
        }

        //save axis input value from this frame
        //hAxisValueLastFrame = GetInputAxis(inputSettings.HorizontalInputAxis);
        //vAxisValueLastFrame = GetInputAxis(inputSettings.VerticalInputAxis);

        //Visualize built up bounce vector
        //Debug.DrawLine(transform.position, (Vector2)transform.position + builtUpBounce * 0.5f * transform.localScale.y, Color.blue, Time.deltaTime, false);

        Debug.DrawLine(transform.position, (Vector2)transform.position + dirInput * 0.5f * transform.localScale.y, Color.green, Time.deltaTime, false);

        // increase counter when having super charge
        if (moveSettings.hasSuperCharge)
        {
            superChargeJumpDurationCounter += Time.deltaTime;
        }
    }

    void FixedUpdate()
    {
        colliding = false;//reset flag
        //contactPoints.Clear();
    }

    // Collected collisions
    public void OnCollisionEnterColl(Collision2D coll, GameObject resolvedObject) 
    {
        // FIXME: this is only for testing purposes


        // check if the collided object is breakable
        // but only if the player really can break the object
        // the is the duration of the super jump is not yet reached
        if (moveSettings.hasSuperCharge &&
            superChargeJumpDurationCounter <= moveSettings.superJumpDurationSec)
        {
            Breakable b = coll.gameObject.GetComponent<Breakable>();

            if (b != null)
            {
                Debug.Log("<color=green>Has Breakable</color>");
            }
        }

    }

    public void OnCollisionExitColl(Collision2D coll, GameObject resolvedObject) {}

    public void OnCollisionStayColl(Collision2D other, GameObject resolvedObject)
    {
        colliding = true;
    }



    //speed up when we are slower than the max movement speed, always apply slow down
    private void applyAcceleration(Rigidbody2D pRB, Vector2 acceleration)
    {
        Vector2 maxSpeed = colliding ? moveSettings.maxWalkSpeed : moveSettings.maxAerialSpeed;

        Vector2 newVelocity = pRB.velocity + acceleration;//new velocity after acceleration without clamping
        Vector2 appliedVelocity = pRB.velocity;//new velocity after acceleration and clamping

        //brake x
        //same direction in x?
        if (Mathf.Sign(acceleration.x) == Mathf.Sign(pRB.velocity.x))
        {
            //faster than max movement speed x?
            if (Mathf.Abs(newVelocity.x) <= maxSpeed.x)
            {
                appliedVelocity.x = newVelocity.x;
            }
        }
        else
        {
            //always apply acceleration if different direction
            appliedVelocity.x = newVelocity.x;
        }

        //brake y
        if (Mathf.Sign(acceleration.y) == Mathf.Sign(pRB.velocity.y))
        {
            if (Mathf.Abs(newVelocity.y) <= maxSpeed.y)
            {
                appliedVelocity.y = newVelocity.y;
            }
        }
        else
        {
            appliedVelocity.y = newVelocity.y;
        }

        /*
        //apply a custom drag if not accelerating & colliding
        if (colliding && acceleration.x == 0.0f)
        {
            appliedVelocity.x -= Mathf.Sign(appliedVelocity.x) * moveSettings.walkingDrag * Time.deltaTime;
        }*/

        pRB.velocity = appliedVelocity;
    }

    private void applyDrag()
    {
        foreach (Rigidbody2D pRB in pointRigidBodies)
        {
            Vector2 v = pRB.velocity;

            v.x -= Mathf.Sign(v.x) * moveSettings.walkingDrag * Time.deltaTime;
            //v.x *= 1.0f - moveSettings.walkingDrag * Time.deltaTime;

            pRB.velocity = v;
        }
    }

    private void OnBounce()
    {
        if (colliding && charge > 0.1f)
        {
            float strength = superChargeTriggered ?
                moveSettings.bounceStrength + moveSettings.bounceStrengthModifier :
                moveSettings.bounceStrength;
            
            Vector2 bounceVel = charge * strength * bounceDir;

            //convert built up bounce into velocity
            foreach (Rigidbody2D pRB in pointRigidBodies)
            {
                pRB.velocity += bounceVel;
            }
        }

        charge = 0.0f;
        massSpring.setConstantForce(Vector2.zero);//stop squash
        isCharging = false;

        // reset super charge values
        superChargeTriggered = false;
        superChargeDelayCounter = 0.0f;
        superChargeJumpDurationCounter = 0.0f;
    }

    private void OnSuperChargeTrigger()
    {
        superChargeTriggered = true;
    }

    //helper methods
    /*
    private Vector2 collisionNormal()
    {
        //calculate average collision normal
        Vector2 avgContactNormal = Vector2.zero;

        foreach (ContactPoint2D c in contactPoints)
        {
            avgContactNormal += c.normal;
        }

        avgContactNormal /= contactPoints.Count;
        Vector2 collisionNormal = avgContactNormal.normalized;

        Debug.DrawLine(transform.position, (Vector2)transform.position + collisionNormal, Color.red, Time.deltaTime, false);//debug visualization

        return collisionNormal;
    }*/

    private Vector2 collisionNormal(Vector2 dirInput)
    {
        Vector2 rayDir = dirInput.normalized;
        rayDir.x *= massSpring.GetScale().x;
        rayDir.y *= massSpring.GetScale().y;

        /*
        RaycastHit2D hit = Physics2D.Raycast(transform.position, rayDir.normalized, rayDir.magnitude, layermask);
        Vector2 normal = Vector2.zero;

        if(hit.collider != null)
        {
            normal = hit.normal;
        }

        if (hit.point == Vector2.zero) { hit.point = transform.position; }*/

        RaycastHit2D hit = new RaycastHit2D();
        Vector2 normal = Vector2.zero;

        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, rayDir.normalized, rayDir.magnitude, layermask);
        foreach (RaycastHit2D h in hits)
        {
            if (!(h.collider is CircleCollider2D))
            {
                normal = h.normal;
                hit = h;
                break;
            }
            else if(hit.collider != null && hit.collider.GetComponent<MassPoint>() == null)
            {
                normal = h.normal;
                hit = h;
                break;
            }
        }


        Debug.DrawLine(transform.position, transform.position + (Vector3)rayDir, Color.cyan, Time.deltaTime, false);//debug visualization
        Debug.DrawLine(hit.point, (Vector2)hit.point + normal, Color.red, Time.deltaTime, false);//debug visualization

        return normal;
    }


    private float GetInputAxis(string axisName)
    {
        return inputSettings.useRawInputAxis ? Input.GetAxisRaw(axisName) : Input.GetAxis(axisName);
    }


    /*
    //Checks whether player has released the directional input
    private bool GetDirInputReleased()
    {

        Vector2 dirInput = new Vector2(GetInputAxis(inputSettings.HorizontalInputAxis), GetInputAxis(inputSettings.VerticalInputAxis));
        Vector2 lastFrameInput = new Vector2(hAxisValueLastFrame, vAxisValueLastFrame);

        //if axis was active last frame and is not active this frame
        if (lastFrameInput.sqrMagnitude > inputSettings.axisReleasedCutoff * inputSettings.axisReleasedCutoff &&
            dirInput.sqrMagnitude <= inputSettings.axisReleasedCutoff * inputSettings.axisReleasedCutoff)
        {
            return true;
        }
        else
        {
            return false;
        }       
    }*/
}
