﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(MassSpringCollisionCollector))]
public class SwallowAbility : MonoBehaviour, MassSpringCollectedCollision
{
    //Parameters
    public float aimSlowMotionFactor = 8.0f;
    public float inhaleRadius = 3.0f;
    public float inhaleStrength = 20.0f;
    public float spitStrength = 100.0f;
    public LayerMask swallowLayer;


    //Input variables
    private Vector2 aimDir = Vector2.zero;
    private float swallowStrength = 0.0f;
    private float swallowStrength_last = 0.0f;

    //Component References
    private MassSpringSystem massSpring;
    private PolygonCollider2D polyCollider;



    //State
    public enum State
    { 
        Idle,
        Inhaling,
        Full,
        Spitting
    }

    [SerializeField]
    private State state = State.Idle;

    //Swallowing
    private List<Swallowable> swallowed = new List<Swallowable>();
    private List<Swallowable> ignoredCollisions = new List<Swallowable>();
    private bool startedSwallowing = false;

    //Slow Motion variables
    private float original_fixedDelta = 0.02f;
    private float original_timeScale = 1.0f;



    //Initialization
	void Start () 
    {
        //set up component references
        massSpring = GetComponent<MassSpringSystem>();
        polyCollider = GetComponent<PolygonCollider2D>();
	}
	
    //Frame Update
	void Update ()
    {
        //capture user input
        Vector2 aimInput = new Vector2(Input.GetAxisRaw("AimX"), Input.GetAxisRaw("AimY"));
        if(aimInput.sqrMagnitude > 0.2f) {  aimDir = aimInput;  }
        swallowStrength = Input.GetButton(InputSwitcher.GetInputForPlatform("Swallow")) ? 1.0f : 0.0f;
        
        


        //Decide what to do based on state
        switch(state)
        {
            case State.Idle:
                {
                    //if swallow button pressed
                    if (swallowStrength > 0.0f)
                    {
                        OnStartInhale();//start inhaling
                    }
                    break;
                }
            case State.Inhaling:
                {
                    //if swallow button released or an object has been swallowed
                    if (swallowed.Count > 0 || swallowStrength_last > 0.0f && swallowStrength <= 0.0f)
                    {
                        OnStopInhale();//stop inhaling
                    }


                    if (aimDir.sqrMagnitude > 0.0f)
                    {
                        Debug.DrawLine(transform.position, transform.position + (Vector3)(inhaleRadius * swallowStrength * aimDir.normalized), Color.yellow);

                        //Draw objects towards player
                        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, 0.5f, aimDir.normalized, inhaleRadius * swallowStrength, swallowLayer);

                        foreach (RaycastHit2D h in hits)
                        {
                            Swallowable toInhale = h.collider.GetComponent<Swallowable>();

                            if(toInhale != null)
                            {
                                toInhale.AddForce(-aimDir.normalized * inhaleStrength * Time.deltaTime * 100.0f);
                                toInhale.InhaleTick(this);
                            }
                        }
                    }

                    break;
                }
            case State.Full:
                {
                    //if swallow button pressed
                    if (swallowStrength_last <= 0.0f && swallowStrength > 0.0f)
                    {
                        OnStartSpitting();//start spitting
                    }
                    else
                    {
                        foreach (Swallowable s in swallowed)
                        {
                            s.SwallowedTick(this);
                        }
                    }
                    break;
                }
            case State.Spitting:
                {
                    //if swallow button released
                    if (swallowStrength_last > 0.0f && swallowStrength <= 0.0f)
                    {
                        OnStopSpitting();//stop spitting
                    }

                    Debug.DrawLine(transform.position, transform.position + (Vector3)(inhaleRadius * swallowStrength * aimDir.normalized), Color.magenta);
                    break;
                }
        }




        //store old input values
        swallowStrength_last = swallowStrength;
	}




    //Action methods
    private void OnStartInhale()
    {
        if(ignoredCollisions.Count == 0)
        {
            state = State.Inhaling;
        }
    }

    private void OnStopInhale()
    {
        if (swallowed.Count > 0) { state = State.Full; }
        else { state = State.Idle; }

        startedSwallowing = false;
        
    }

    private void OnStartSpitting()
    {
        state = State.Spitting;

        //slow down time while aiming
        original_fixedDelta = Time.fixedDeltaTime;
        original_timeScale = Time.timeScale;//save original values to restore later

        Time.timeScale /= aimSlowMotionFactor;
        Time.fixedDeltaTime /= aimSlowMotionFactor;       

    }

    private void OnStopSpitting()
    {
        state = State.Idle;

        //restore normal game speed
        Time.timeScale = original_timeScale;
        Time.fixedDeltaTime = original_fixedDelta;


        //Restore all Swallowed gameObjects (with delay in between)
        float spitTimer = 0.0f;

        foreach (Swallowable s in swallowed)
        {
            StartCoroutine(s.SpitRoutine(this, aimDir, spitTimer));
            spitTimer += 0.15f;

            //readjust player scale
            Vector2 currentScale = massSpring.GetScale();
            massSpring.SetScale(currentScale - s.sizeModifier);
        }

        swallowed.Clear();//empty list
    }





    //Point Collision Methods
    public void OnCollisionStayColl(Collision2D coll,GameObject resolvedObject)
    {
        if (startedSwallowing) { return; }//dont swallow another if a swallow coroutine has already been started

        //If inhaling and a swallowable object is touched
        if (state == State.Inhaling && ((1 << coll.gameObject.layer) & swallowLayer.value) > 0)
        {

            GameObject swallowObject = coll.gameObject;
            MassPoint massPoint = swallowObject.GetComponent<MassPoint>();

            if (massPoint != null) { swallowObject = massPoint.GetSystem().gameObject; }


            //only swallow object if within aim
            if (Vector2.Dot(swallowObject.transform.position - transform.position, aimDir) > 0.8f)
            {
                Swallowable toSwallow = swallowObject.GetComponent<Swallowable>();

                //if not already swallowed
                if (toSwallow != null && !swallowed.Contains(toSwallow) && toSwallow.canSwallow)
                {
                    startedSwallowing = true;
                    StartCoroutine(toSwallow.SwallowRoutine(this));
                }
            }


        }
    }

    public void OnCollisionExitColl(Collision2D coll,GameObject resolvedObject) {}
    public void OnCollisionEnterColl(Collision2D coll,GameObject resolvedObject) {}



    //Public Access
    public List<Swallowable> GetSwallowedObjects()
    {
        return swallowed;
    }

    public MassSpringSystem GetMassSpringSystem()
    {
        return massSpring;
    }

    public PolygonCollider2D GetPolygonCollider()
    {
        return polyCollider;
    }

    public Vector2 GetCurrentAimDirection()
    {
        return aimDir;
    }

    public State GetState()
    {
        return state;
    }

    public List<Swallowable> GetIgnoredCollisions()
    {
        return ignoredCollisions;
    }

    public void SpitOut()
    {
        if (state == State.Full)
        {
            OnStopSpitting();
        }
    }

    public void SpitOut(Vector2 dir)
    {
        if (state == State.Full)
        {
            aimDir = dir;
            OnStopSpitting();
        }
    }
}
