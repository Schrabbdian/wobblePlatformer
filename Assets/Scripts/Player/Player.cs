﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a Player and its fundamental attributes (health, score, etc.)
/// </summary>
/// <remarks>
/// This class is not responsible for the Player's own movement, abilities or animation (those have separate scripts).
/// </remarks>
public class Player : MonoBehaviour, MassSpringCollectedCollision
{
    //Parameters
    public float KnockbackStrength = 15.0f;

    //Component References
    private MassSpringSystem massSpring;
    private SwallowAbility swallow;

    //Health and Knockback
    private int health = 3;
    private bool knockedBack = false;

    //Initialization
    void Start()
    {
        massSpring = GetComponent<MassSpringSystem>();
        swallow = GetComponent<SwallowAbility>();
    }

    public void Damage(int amount)
    {
        SetHealth(health - amount);//deal damage to health

        if (health <= 0)
        {
            OnDeath();
        }
    }

    void OnDeath()
    {
        Debug.Log("Ded");
    }

    //Collision Methods   
    public void OnCollisionEnterColl(Collision2D coll, GameObject resolvedObject) 
    {
        if (resolvedObject.CompareTag("Pointy") && !knockedBack)
        {
            Vector2 knockbackDir = Vector2.zero;
            foreach (var p in coll.contacts)
            {
                knockbackDir += p.normal;
            }
            knockbackDir /= coll.contacts.Length;

            knockbackDir = knockbackDir.normalized;
            
            KnockBack(knockbackDir);
        }
    }

    public void OnCollisionExitColl(Collision2D coll, GameObject resolvedObject) { }
    public void OnCollisionStayColl(Collision2D col, GameObject resolvedObject) { }

    //Knockback
    public void KnockBack(Vector2 knockbackDir)
    {
        this.Damage(1);
        StartCoroutine(knockBackRoutine(knockbackDir));
    }

    private IEnumerator knockBackRoutine(Vector2 knockbackDir)
    {
        knockedBack = true;

        swallow.SpitOut(knockbackDir);

        massSpring.AddForce(knockbackDir * KnockbackStrength * massSpring.mass, ForceMode2D.Impulse);

        massSpring.SetScale((Vector2)massSpring.GetScale() + Vector2.one * 0.5f);

        yield return new WaitForSeconds(0.15f);

        massSpring.SetScale((Vector2)massSpring.GetScale() - Vector2.one * 0.5f);

        knockedBack = false;
    }

    //Public Access
    public void SetHealth(int h)
    {
        health = h;
    }

    public int GetHealth()
    {
        return health;
    }
}
