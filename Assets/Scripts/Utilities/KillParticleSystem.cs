﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class KillParticleSystem : MonoBehaviour 
{
    private ParticleSystem partSys;

	// Use this for initialization
	void Start () 
    {
        partSys = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!partSys.IsAlive())
        {
            Destroy(gameObject);
        }
		
	}
}
