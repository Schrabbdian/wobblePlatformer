﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColorUtil
{
    /// <summary>
    /// Interpolates between two colors linearly including the alpha
    /// </summary>
    /// <param name="a">Color when t = 0</param>
    /// <param name="b">Color when t = 1</param>
    /// <param name="t"></param>
    /// <returns></returns>
    public static Color LerpWithAlpha(Color a, Color b, float t)
    {
        t = Mathf.Clamp01(t);

        Color c;
        c.r = Mathf.Lerp(a.r, b.r, t);
        c.g = Mathf.Lerp(a.g, b.g, t);
        c.b = Mathf.Lerp(a.b, b.b, t);
        c.a = Mathf.Lerp(a.a, b.a, t);

        return c;
    }
}