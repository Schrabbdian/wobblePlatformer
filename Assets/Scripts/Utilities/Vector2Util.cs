﻿using UnityEngine;
using System.Collections;

public class Vector2Util 
{
    /// <summary>
    /// Calculates Clockwise Angle in [0.0, 360.0) from Vector a to b
    /// </summary>
    /// <param name="a">from Vector</param>
    /// <param name="b">to Vector</param>
    /// <returns>Angle in [0.0, 360.0´)</returns>
    public static float clockwiseAngle(Vector2 a, Vector2 b)
    {
        float angle = Vector2.Angle(a, b);

        float sign = Mathf.Sign(a.x * b.y - a.y * b.x);

        if (sign == -1) { angle = 360.0f - angle; } 
        return angle;
    }

    /// <summary>
    /// Calculates signed Angle in [-180.0, 180.0) from Vector a to b
    /// </summary>
    /// <param name="a">from Vector</param>
    /// <param name="b">to Vector</param>
    /// <returns>Angle in [-180.0, 180.0)</returns>
    public static float signedAngle(Vector2 a, Vector2 b)
    {
        float angle = Vector2.Angle(a, b);

        float sign = Mathf.Sign(a.x * b.y - a.y * b.x);

        return sign * angle;
    }

    public static Vector2 RotateVec2(Vector2 v, float angle)
    {
        return Quaternion.AngleAxis(angle, Vector3.back) * v;
    }

    public static Vector2 Project(Vector2 v, Vector2 onto)
    {
        return onto.normalized * Vector2.Dot(v, onto.normalized);
    }

    public static float ProjectScalar(Vector2 v, Vector2 onto)
    {
        return Vector2.Dot(v, onto.normalized);
    }
}
