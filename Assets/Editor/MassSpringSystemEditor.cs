﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MassSpringSystem))]
public class MassSpringSystemEditor : Editor
{
    //Editor colors
    private readonly Color jointColor = Color.green;
    private readonly Color massPointColor = Color.red;
    private readonly Color anchorColor = Color.magenta;

    //editor gizmos
    private readonly Vector3 cube = new Vector3(0.2f, 0.2f, 0);

    private readonly float massPointDiameter = 0.2f;

    private MassSpringSystem massSpring;

    public void OnEnable()
    {
        massSpring = (MassSpringSystem) target;
    }

    private void OnSceneGUI()
    {
        if (Selection.activeTransform != massSpring.transform)
        {
            return;
        }


        //draw Springs
        Handles.color = jointColor;
        foreach (SpringJoint2D spring in massSpring.GetSprings())
        {
            Handles.DrawLine(spring.transform.position,
                spring.connectedBody.position); //possibly use anchor and connected anchor local to world positions
        }

        //draw points
        foreach (Transform point in massSpring.MassPoints)
        {
            Handles.color = massPointColor;
            Handles.DrawSolidDisc(point.position, Vector3.back, massPointDiameter / 2);
            Handles.color = Color.black;
            Handles.DrawWireDisc(point.position, Vector3.back, massPointDiameter / 2);
        }

        //draw anchors
        Handles.color = anchorColor;

        foreach (Transform anchorPoint in massSpring.AnchorPoints)
        {
            Handles.DrawSolidRectangleWithOutline(new Rect(anchorPoint.position - cube / 2, cube),
                anchorColor, Color.black);
        }
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Show mass points in hierarchy"))
        {
            ShowMassPointsInHierarchy(true);
        }
        /*if (GUILayout.Button("Hide"))
        {
            ShowMassPointsInHierarchy(false);
        }*/
        GUILayout.EndHorizontal();
    }

    private void ShowMassPointsInHierarchy(bool show)
    {
        if (show)
        {
            foreach (Transform point in massSpring.MassPoints)
            {
                point.gameObject.hideFlags = HideFlags.None;
            }
        }
        else
        {
            foreach (Transform point in massSpring.MassPoints)
            {
                point.gameObject.hideFlags = HideFlags.HideInHierarchy;
            }
        }
    }
}