﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

//Author: Robert, using a Catlikecoding.com tutorial

//a custom editor for bezier splines
[CustomEditor(typeof(BezierSpline))]
public class BezierSplineEditor : Editor
{
    private BezierSpline spline;
    private Quaternion handleRot;

    private int selected = -1;//selected point index in the spline's point array
    private const float handleSize = 0.05f;
    private const float pickSize = 0.08f;

    private static Color[] modeColors = {
		Color.white,
		Color.green,
		Color.cyan
	};

    //Collider automation
    private float offset;
    private int resolution;


    private void OnSceneGUI()
    {
        spline = (BezierSpline)target;//the spline we are currently editing
        handleRot = (Tools.pivotRotation == PivotRotation.Local) ? spline.transform.rotation : Quaternion.identity;


        //update & draw all points in the spline

        Vector3 p0 = ShowPoint(0);//start with first point
        for (int i = 1; i < spline.ControlPointCount; i += 3)
        {
            //update points based on handle transformation
            Vector3 p1 = ShowPoint(i);
            Vector3 p2 = ShowPoint(i + 1);
            Vector3 p3 = ShowPoint(i + 2);

            //draw tangent handles
            Handles.color = Color.gray;
            Handles.DrawLine(p0, p1);
            Handles.DrawLine(p2, p3);

            //draw spline
            Handles.DrawBezier(p0, p3, p1, p2, Color.white, null, 2f);

            p0 = p3;//for the next curve (=iteration), use this one's last point as the first
        }
    }

    public override void OnInspectorGUI()
    {
        spline = (BezierSpline)target;//the spline we are currently editing

        //add checkbox for loop property in inspector
        EditorGUI.BeginChangeCheck();
        bool loop = EditorGUILayout.Toggle("Loop", spline.Loop);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(spline, "Toggle Loop");         
            spline.Loop = loop;
            EditorUtility.SetDirty(spline);
        }

        //visualize selected point in inspector
        if (selected > 0 && selected < spline.ControlPointCount) { DrawSelectedPointInspector(); }

        //Add button "Add Waypoint"
        if (GUILayout.Button("Add Waypoint"))
        {
            Undo.RecordObject(spline, "Add Waypoint");//record state before for undoing

            spline.AddCurve();//adds 3 new points to the spline

            EditorUtility.SetDirty(spline);//signal change
        }

        // Add button "Remove Waypoint"
        // but only if there is a point selected
        if (selected % 3 == 0 && GUILayout.Button("Remove Waypoint"))
        {
            Undo.RecordObject(spline, "Remove Waypoint");
            spline.RemoveCurve(selected);
            selected = -1;
            EditorUtility.SetDirty(spline);
        }

        //Collider Automation properties
        GUILayout.Label("Add generated EdgeCollider2D to spline");
        offset = EditorGUILayout.FloatField("Offset", offset);
        resolution = EditorGUILayout.IntField("Resolution", resolution);

        if(GUILayout.Button("Generate EdgeCollider2D"))
        {
            EdgeColliderContextEditor.CalculateCollider(spline, offset, resolution);
        }
    }

    private Vector3 ShowPoint(int index)
    {
        Vector3 point = spline.transform.TransformPoint(spline.GetControlPoint(index));

        //create a handle button
        Handles.color = modeColors[(int)spline.GetControlPointMode(index)];//color handle according to mode
        float size = HandleUtility.GetHandleSize(point);

        if (index == 0) { size *= 2.0f; }//make start twice as big

        if (index % 3 == 0 && Handles.Button(point, handleRot, handleSize * size, pickSize * size, Handles.DotCap))
        {
            selected = index;//button selects this point
            Repaint();//signals the editor that it should be updated
        }
        else if (index % 3 != 0 && Handles.Button(point, handleRot, handleSize * size, pickSize * size, Handles.CircleCap))
        {
            selected = index;//button selects this point
            Repaint();//signals the editor that it should be updated
        }

        //if point selected
        if (selected == index)
        {
            //update point based on handle change
            EditorGUI.BeginChangeCheck();
            point = Handles.DoPositionHandle(point, handleRot);//record how the handle is moved
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(spline, "Move Point");//record state before action (for undoing)

                spline.SetControlPoint(index, spline.transform.InverseTransformPoint(point));//set point to moved handle's transform

                EditorUtility.SetDirty(spline);//signal a change has been made
            }
        }

        return point;
    }

    private void DrawSelectedPointInspector()
    {
        GUILayout.Label("Selected Point");

        //draw interactive position
		EditorGUI.BeginChangeCheck();
		Vector3 point = EditorGUILayout.Vector3Field("Position", spline.GetControlPoint(selected));
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(spline, "Move Point");
            EditorUtility.SetDirty(spline);
            spline.SetControlPoint(selected, point);
        }

        //draw interactive mode selector
        EditorGUI.BeginChangeCheck();
        BezierControlPointMode mode = (BezierControlPointMode)
            EditorGUILayout.EnumPopup("Mode", spline.GetControlPointMode(selected));
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(spline, "Change Point Mode");
            spline.SetControlPointMode(selected, mode);
            EditorUtility.SetDirty(spline);
        }
    }
}
