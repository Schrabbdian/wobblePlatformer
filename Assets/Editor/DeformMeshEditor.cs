﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DeformMesh))]
public class DeformMeshEditor : Editor
{
    private readonly Color cagePointColor = Color.red;

    private readonly float cagePointDiameter = 0.2f;

    private DeformMesh mesh;
    
    public void OnEnable()
    {
        mesh = (DeformMesh) target;
    }

    private void OnSceneGUI()
    {
        //dont draw if not selected
        Transform t = Selection.activeTransform;
        if (t != mesh.transform)
        {
            
            return;
        }
        //dont draw if a massspringsystem exists on this gameobject
        else if(t.GetComponent<MassSpringSystem>())
        {
            return;
        }

        //draw points
        foreach (Transform point in mesh.cagePoints)
        {
            Handles.color = cagePointColor;
            Handles.DrawSolidDisc(point.position, Vector3.back, cagePointDiameter / 2);
            Handles.color = Color.black;
            Handles.DrawWireDisc(point.position, Vector3.back, cagePointDiameter / 2);
        }
    }


    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Show mass points in hierarchy"))
        {
            ShowMassPointsInHierarchy(true);
        }
        /*if (GUILayout.Button("Hide"))
        {
            ShowMassPointsInHierarchy(false);
        }*/
        GUILayout.EndHorizontal();
    }

    private void ShowMassPointsInHierarchy(bool show)
    {
        if (show)
        {
            foreach (Transform point in mesh.cagePoints)
            {
                point.gameObject.hideFlags = HideFlags.None;
            }
        }
        else
        {
            foreach (Transform point in mesh.cagePoints)
            {
                point.gameObject.hideFlags = HideFlags.HideInHierarchy;
            }
        }
    }
}
