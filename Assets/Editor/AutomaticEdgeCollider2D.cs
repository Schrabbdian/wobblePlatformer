﻿using UnityEngine;

public class EdgeColliderContextEditor {

    //width: offset from splineresolution: amount of points between start and end point
    public static void CalculateCollider(BezierSpline spline, float width, int resolution)
    {
        AddSplinedEdgeCollider2D(spline, width, resolution);
        Debug.Log("Created EdgeCollider2D from Spline. Offset: " + width + ", Resolution: " + resolution);
    }

    private static void AddSplinedEdgeCollider2D (BezierSpline spline, float width, int resolution)
    {
        //ensure valid input
        if (resolution < 0)
            resolution = 0;

        int numOfPoints = resolution + 2;
        Vector2[] points = new Vector2[numOfPoints];

        //Middle Points
        float step = 1f / (resolution + 1); //if we want 11 middlepoints we need to go 1/12th of the way for each middle point
        float progress = 0;

        for (int i = 0; i < numOfPoints; i++)
        {
            Vector2 vel = spline.GetVelocity(progress);
            points[i] = spline.transform.InverseTransformPoint(spline.GetPoint(progress) + new Vector3(vel.y, -vel.x).normalized * width);
            progress += step;
        }

        //Add Collider
        EdgeCollider2D coll = GetCollider(spline);
        coll.points = points;
    }

    //get collider. if not, create one
    private static EdgeCollider2D GetCollider(BezierSpline spline)
    {
        EdgeCollider2D coll = spline.gameObject.GetComponent<EdgeCollider2D>();
        if (!coll)
        {
            coll = spline.gameObject.AddComponent<EdgeCollider2D>();
        }

        return coll;
    }

    //return distance between first collider point and first spline point
    public static float GetColliderOffset(BezierSpline spline)
    {
        EdgeCollider2D e = spline.GetComponent<EdgeCollider2D>();

        if (!e || e.points.Length == 0) return 0;

        Vector2 collStart = e.points[0];
        Vector3 splineStart = spline.GetPoint(0);

        return Vector2.Distance(collStart, new Vector2(splineStart.x, splineStart.y));
    }

    public static int GetColliderResolution(BezierSpline spline)
    {
        EdgeCollider2D e = spline.GetComponent<EdgeCollider2D>();

        if (!e) return 0;

        int value = e.points.Length - 2;

        return (value < 0) ? 0 : value;
    }
}
